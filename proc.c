/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "includes.h"

SDL_Surface *sub, *left, *right;
SDL_Rect sub_rec, dst_rec;

Sint32 current_scale;
Sint32 obj_x, obj_y;
cursor_t cursor;

float zoom;
float globl_time = 0.0f;

const color_t sub_bg = { 0x3f, 0x3f, 0x3f };

track_t tracks[TRACKS];


#define Y \
 (Sint32)(((float)vert->param.d1 / 256.0f) * (float)((TRACKS*TRACK_SIZE+4)-main_obj->param.h))

double m(double x,double y) { 
 double a; 
 return ((a=x/y)-(Sint32)a)*y; 
}


/* clip_rect {{{ */
Sint32 clip_rect(SDL_Rect *bound, SDL_Rect *in) {
 Sint32 y,h;
 y = in->y;
 h = in->h;
 if(bound->y > in->y) {
  y+= (bound->y-in->y);
  h-= (bound->y-in->y);
  if(h <=0) return 0;
 }
 if((bound->y+bound->h) < (in->y+in->h)){
  h-= ( (in->y+in->h) - (bound->y+bound->h));
  if(h<=0) return 0;
 }
 in->y = y;
 in->h = h;
}
/* }}} */
/* sub_update_block {{{ */
void sub_update_block(Sint32 x1, Sint32 y1, Sint32 x2, Sint32 y2) {
 SDL_Rect bounding, dst;
 Sint32 unclipped_y;
 dst.x = x1 + main_obj->param.x;
 bounding.x = x1;
 dst.w = bounding.w = (x2-x1) + 1;
 dst.h = 
 bounding.h = (y2-y1) +1;
 bounding.y = y1; 
 dst.y = y1 + main_obj->param.y - Y;

 
 unclipped_y = dst.y;
 if(clip_rect(&dst_rec, &dst)!=0) {
  bounding.y += (dst.y - unclipped_y);
  bounding.h = dst.h;
  PROT;
  SDL_BlitSurface(sub, &bounding, gui_screen, &dst); 
  SDL_UpdateRect(gui_screen, dst.x,dst.y,dst.w,dst.h);
  UNPROT;
 } 

}
/* }}} */
/* sub_update {{{ */
void sub_update(struct object_t *obj) {
 sub_update_block(obj->param.x, obj->param.y,
   obj->param.x+obj->param.w,obj->param.y+obj->param.h);
}
/* }}} */

void norm_update(struct object_t *obj) {
// printf("norm update\n");
 UPDATE_OBJECT(obj);
}

/* proc_main {{{ */
Sint32 proc_main(Sint32 msg, struct object_t *obj, Sint32 data) {
 struct object_t *walker;
 group_t *old;

 Sint32 store_gui_mouse_x, store_gui_mouse_y;

 switch(msg) {
  case MSG_START:

   if((sub = SDL_CreateRGBSurface(SDL_SWSURFACE,obj->param.w, (TRACK_SIZE*TRACKS+4),24,
       gui_screen->format->Rmask, gui_screen->format->Gmask,
       gui_screen->format->Bmask, gui_screen->format->Amask))<=0) {
    printf("bunko\n");
    exit(-1);
   }
   printf("sub: 0x%x\n", sub);
   sub_rec.w = obj->param.w;
   sub_rec.x = 0;
   sub_rec.y = 0;

   dst_rec.w = obj->param.w;
   dst_rec.h = obj->param.h;
   dst_rec.x = obj->param.x;
   dst_rec.y = obj->param.y;
   walker = sub_grp->objs;
   for(;;) {
    walker->in_focus = 0;
    walker->clicked = FALSE;
    if((walker = (struct object_t *)walker->node.next)==sub_grp->objs) break;
   }
   broadcast_group(sub_grp, MSG_START, 0);
   MESSAGE_OBJECT(obj, MSG_USER);
   break;
  case MSG_OUTFOCUS:
   POP_SUB;
   walker = sub_grp->objs;
   for(;;) {
    if(walker->in_focus == 1) {
     walker->in_focus = 0;
     MESSAGE_OBJECT(walker, MSG_OUTFOCUS);
    }
    if((walker = (struct object_t *)walker->node.next) == sub_grp->objs) break;
   }
   PUSH;
   break;
  case MSG_USER:
   sub_rec.h = sub->h;
 
   POP_SUB;
   SDL_FillRect( sub, &sub_rec, SDL_MapRGB(gui_screen->format, sub_bg.r, sub_bg.g, sub_bg.b));
   sub_rec.h = obj->param.h;

   walker = sub_grp->objs;
   for(;;) {
    if(walker->param.proc == proc_shadow_box)
     MESSAGE_OBJECT(walker, MSG_DRAW);
    if((walker=(struct object_t *)walker->node.next) == sub_grp->objs) break;
   }
   walker = sub_grp->objs;
   for(;;) {
    if(walker->param.proc != proc_shadow_box)
     MESSAGE_OBJECT(walker, MSG_DRAW);
    if((walker=(struct object_t *)walker->node.next) == sub_grp->objs) break;
   }

//   broadcast_group(sub_grp, MSG_DRAW,0);
   PUSH;
   break;
  case MSG_DRAW:
   sub_rec.y = Y;
   PROT;
   SDL_BlitSurface(sub, &sub_rec, gui_screen, &dst_rec); 
   UNPROT;
   break;
  case MSG_MOUSEMOVE:
   POP_SUB;
   walker = sub_grp->objs; 
   for(;;) { 
    if(gui_mouse_x >= (walker->param.x + obj->param.x) && 
       gui_mouse_x <= (walker->param.x + obj->param.x + walker->param.w) && 
       gui_mouse_y >= (walker->param.y + obj->param.y - Y) && 
       gui_mouse_y <= (walker->param.y + obj->param.y + walker->param.h - Y)) { 
  
     if(walker->in_focus == 0) {
      walker->in_focus = 1;
      MESSAGE_OBJECT(walker, MSG_INFOCUS);
     } 
    obj_x = gui_mouse_x - obj->param.x - walker->param.x;
    obj_y = gui_mouse_y - obj->param.y - walker->param.y + Y;
    MESSAGE_OBJECT(walker, MSG_MOUSEMOVE);


    } else 
     if(walker->in_focus == 1) {
      walker->in_focus = 0;
      MESSAGE_OBJECT(walker, MSG_OUTFOCUS);
     } 

    if( (walker = (struct object_t *)walker->node.next) == sub_grp->objs) break; 
   }

   PUSH;
   break;

  case MSG_UNCLICK:
  case MSG_CLICK:
  case MSG_RIGHT:
  case MSG_KEYDOWN:
  case MSG_KEYUP:
   walker = sub_grp->objs; 
   store_gui_mouse_x = gui_mouse_x;
   store_gui_mouse_y = gui_mouse_y;
   for(;;) { 
    if(store_gui_mouse_x >= (walker->param.x + obj->param.x) && 
       store_gui_mouse_x <= (walker->param.x + obj->param.x + walker->param.w) && 
       store_gui_mouse_y >= (walker->param.y + obj->param.y - Y) && 
       store_gui_mouse_y <= (walker->param.y + obj->param.y + walker->param.h - Y)) { 
 
     POP_SUB;
     if(msg == MSG_CLICK) 
      walker->clicked == TRUE; 

     if(msg == MSG_UNCLICK && walker->clicked == TRUE) {
      MESSAGE_OBJECT(walker, MSG_PRESS);
      walker->clicked = FALSE;
     }
     walker->param.proc(msg,walker, data);
     PUSH;
    } 
    if( (walker = (struct object_t *)walker->node.next) == sub_grp->objs) break; 
   }

   break;
 }
 return RET_OK;
}
/* }}} */

/* proc_wave {{{ */
Sint32 proc_wave(Sint32 msg, struct object_t *obj, Sint32 data) {
 SDL_Rect src, dst;
 struct object_t *old_obj;
 Sint32 i,color, k;
 Sint32 up_left, up_right;
 double *store;
 double old_pos, old_pos2, q;
 Sint32 x, px, begin, end;
 switch(msg) {
  case MSG_RIGHT:
   menu_wave_grp->pos_x = gui_mouse_x - 6;
   menu_wave_grp->pos_y = gui_mouse_y - 6;
   POP_MAIN;
   group_loop(menu_wave_grp);
   PUSH;
   POP_TIMEBAR;
   MESSAGE_OBJECT(time_bar, MSG_DRAW);
   PUSH;
   POP_SUB;
   for(i = 0;i<TRACKS;i++) {
    MESSAGE_OBJECT(tracks[i].wave, MSG_DRAW);
    sub_update(tracks[i].wave);
   }
   PUSH;
   break;
  case MSG_START:
   broadcast_group(time_bar_grp, MSG_DRAW,0);
   break;
   obj->param.d1 = -40;
  case MSG_MOUSEMOVE:
   if((obj_x <20) ||
      (obj_x > (obj->param.w-20)))
      break;

   hline(obj_x+obj->param.x-5, obj->param.y+obj_y,
         obj_x+obj->param.x+5, &globl_fg, &globl_bg, XOR);
   vline(obj_x+obj->param.x, obj->param.y, 
     obj_y+obj->param.y+obj->param.h, &globl_fg, &globl_bg, XOR);


   up_left = obj_x-(5+abs(obj->param.d1-obj_x));
   up_right =obj_x+(5+abs(obj->param.d1-obj_x));
   if(up_left<0)
    up_left = 0;
   if(up_right>obj->param.w)
    up_right = obj->param.w;


   sub_update_block( obj->param.x+up_left, obj->param.y, 
                     obj->param.x+up_right, obj->param.y+obj->param.h);
   vline(obj_x+obj->param.x, obj->param.y, 
     obj_y+obj->param.y+obj->param.h, &globl_fg, &globl_bg, XOR);
   hline(obj_x+obj->param.x-5, obj->param.y+obj_y,
         obj_x+obj->param.x+5, &globl_fg, &globl_bg, XOR);
   obj->param.d1 = obj_x;


   break;

#define IS_IN_TIMEBAR \
   if(obj_y >= ((obj->param.h/2) - (time_bar->param.h/2)) &&\
      obj_y <= ((obj->param.h/2) + (time_bar->param.h/2))) 

  case MSG_KEYDOWN:
   IS_IN_TIMEBAR {
    time_bar->param.proc(MSG_KEYDOWN, time_bar, data);
    break;
   }
   break;
  case MSG_CLICK:
   if((obj_x <=20) ||
      (obj_x >= (obj->param.w-20))) {
    if(obj_x <=20)
     i = 0;
    else
     i = obj->param.w - 20;
    fill_box(obj->param.x+i, obj->param.y, obj->param.x+20+i, obj->param.y+obj->param.h,
      &globl_fg, &globl_bg, XOR);
    sub_update_block(obj->param.x+i, obj->param.y, obj->param.x+20+i,obj->param.y+obj->param.h);
    while(wait_on_mouse()!=MOUSE_UP);
    fill_box(obj->param.x+i, obj->param.y, obj->param.x+20+i, obj->param.y+obj->param.h,
      &globl_fg, &globl_bg, XOR);
    sub_update_block(obj->param.x+i, obj->param.y, obj->param.x+20+i,obj->param.y+obj->param.h);
    mover(i==0 ? CMD_PAGELEFT:CMD_PAGERIGHT);
    break;
   }
   IS_IN_TIMEBAR {
    MESSAGE_OBJECT(time_bar, MSG_CLICK);
    break;
   }
#define POS \
   (((((float)obj_x-20)/(time_bar->param.w/5))/zoom)+globl_time)
   old_obj = tracks[cursor.track].wave;
   tracks[cursor.track].cue->param.flags |= FADE;
   MESSAGE_OBJECT(tracks[cursor.track].cue, MSG_DRAW);
   cursor.track = obj->param.d2;
   tracks[cursor.track].cue->param.flags &= (~0^FADE);
   MESSAGE_OBJECT(tracks[cursor.track].cue, MSG_DRAW);
   cursor.pos = POS;
   x = gui_mouse_x;
   cursor.end = POS;


   MESSAGE_OBJECT(obj, MSG_DRAW);
   MESSAGE_OBJECT(old_obj, MSG_DRAW);
#define JPOS \
 (((((float)(obj_x+px)-20)/(time_bar->param.w/5))/zoom)+globl_time)
   old_pos = cursor.pos;
   for(;;) {
    px = gui_mouse_x - x;
    if(JPOS < old_pos) { 
     store = &cursor.pos;
     cursor.end = old_pos;
    } else { 
     store = &cursor.end;
     cursor.pos = old_pos;
    }
    if(*store!= JPOS) 
     MESSAGE_OBJECT(obj, MSG_DRAW);
    *store = JPOS;
    if(wait_on_mouse() == MOUSE_UP) break;
   }
#undef JPOS
#undef POS
   printf("hello\n");
   break;
#undef IS_IN_TIMEBAR
  case MSG_DRAW:
   if((obj->param.y + obj->param.h) < Y ||
      (obj->param.y > (Y + main_obj->param.h))) { 
    tracks[obj->param.d2].dirty = 1;
    break;
   }
  case MSG_CLEAN:
   if(obj->in_focus == 0) 
    color = SDL_MapRGB(sub->format, (float)globl_bg.r*0.84f, (float)globl_bg.g*0.84f, (float)globl_bg.b*0.84f);
   else
    color = SDL_MapRGB(sub->format, globl_bg.r, globl_bg.g, globl_bg.b);

   dst.x = obj->param.x+20;
   dst.y = obj->param.y;
   dst.w = obj->param.w-40;
   dst.h = obj->param.h;

   SDL_FillRect(sub, &dst, color);

   if(obj->param.d2 == cursor.track) {
#define POS(X) \
  ((obj->param.x+20) + (( X - globl_time) * zoom * (float)time_bar->param.w/5.0f))

    begin = POS(cursor.pos);
    end = POS(cursor.end);

    if(end > (obj->param.x + 20) &&
       begin < ((obj->param.x + obj->param.w) - 20)) {

     dst.x = begin;
     dst.y = obj->param.y;
     dst.w = end - begin;
     dst.h = obj->param.h;

     if(dst.x <= (obj->param.x+20)) {
      dst.w -= (obj->param.x + 20) - dst.x;
      dst.x = obj->param.x+20;
     }

     if((dst.x+ dst.w) >= ((obj->param.x + obj->param.w) - 20)) {
      dst.w -= (dst.x+dst.w) - ((obj->param.x + obj->param.w) - 20); 
     } 

     SDL_FillRect(sub, &dst, SDL_MapRGB(sub->format, 0xee, 0xee, 0xe2)); 
#define REPEAT(X) \
     if(X >= obj->param.x+20 && \
        X <= (obj->param.x+obj->param.w)-20) \
      vline(X, obj->param.y, obj->param.y+obj->param.h, \
        obj->param.fg, obj->param.bg, NO_HASH);
     REPEAT(begin);
     REPEAT(end);
#undef REPEAT
    }
   } 
#undef POS


   src.x = 0; src.y = 0; 
   dst.w = src.w=20; dst.h = src.h = 169;
   dst.x = obj->param.x;
   dst.y = obj->param.y;
   SDL_BlitSurface(left, &src, sub, &dst);
   dst.x = obj->param.x+obj->param.w -20;
   SDL_BlitSurface(right, &src, sub, &dst);

   src.x = time_bar->param.x; src.y = time_bar->param.y;
   src.w = time_bar->param.w; src.h = time_bar->param.h;
   dst.x = obj->param.x+20; dst.y =obj->param.y+( (obj->param.h/2) - (time_bar->param.h/2));
   dst.w = time_bar->param.w; dst.h = time_bar->param.h;
   SDL_BlitSurface(time_bar_bmp, &src, sub, &dst); 
   tracks[obj->param.d2].dirty = 0;
   sub_update(obj);
   break;
  case MSG_OUTFOCUS:
  case MSG_INFOCUS:
   MESSAGE_OBJECT(obj, MSG_DRAW);
   break;
 }
 return RET_OK;
}
/* }}} */


/* floats give us rounding problems when it comes to 
 * day-scale times, no foolin' huh */

/* do_line {{{ */
Sint32 do_line(struct object_t *obj, double scale, char s) {
 Sint32 i, j, k;
 double div;
 char buf[MAX_CHARS];
 color_t color;
 double store, store2;
 Sint32 clip;

#define SPACER \
 (double)obj->param.x+(div*(double)i) - \
 m(  ((globl_time/scale)*div), div )
 
 div = ((((double)obj->param.w*zoom)/5.0f)*scale);


 if(div<4.0f) return;
 if(div>(obj->param.w*4)) return;
 store = (div / ((double)obj->param.w/5.0f));
 if(store < 0.0f)
  store = 0.0f;
 if(store > 1.0f)
  store = 1.0f;

 if(store > 0.9f) 
  switch(s) {
   case '~':
    current_scale = CHUNKS;
    break;
   case 's':
    current_scale = SECONDS;
    break;
   case 'm':
    current_scale = MINUTES;
    break;
   case 'h':
    current_scale = HOURS;
    break;
   case 'd':
    current_scale = DAYS;
    break;
  }

 store2 = obj->param.bg->r -obj->param.fg->r;
 color.r  = obj->param.bg->r - (Sint32)(store2 * store);


 store2 = obj->param.bg->g -obj->param.fg->g;
 color.g  = obj->param.bg->g - (Sint32)(store2 * store);

 store2 = obj->param.bg->b -obj->param.fg->b;
 color.b  = obj->param.bg->b - (Sint32)(store2 * store);


 j = (obj->param.w / div)+2;

 for(i=-1;i<j;i++) {  
  if(SPACER > obj->param.x+1 && 
     SPACER < (obj->param.x+obj->param.w)) 
   vline(SPACER, obj->param.y+1, (obj->param.y+obj->param.h-9), 
    &color, obj->param.bg, NO_HASH); 
  if((SPACER+((Sint32)div>>1)) > obj->param.x+1 && 
     (SPACER+((Sint32)div>>1)) < (obj->param.x+obj->param.w)) 
   vline(SPACER+((Sint32)div>>1), obj->param.y+5, (obj->param.y+obj->param.h)-5, 
    &color, obj->param.bg, NO_HASH); 
  if((SPACER+((Sint32)div>>2)) > obj->param.x+1 && 
     (SPACER+((Sint32)div>>2)) < (obj->param.x+obj->param.w)) 
   vline(SPACER+((Sint32)div>>2), obj->param.y+10, (obj->param.y+obj->param.h)-10, 
    &color, obj->param.bg, NO_HASH); 
  if(((SPACER+((Sint32)div>>2))+((Sint32)div>>1)) > obj->param.x+1 && 
     ((SPACER+((Sint32)div>>2))+((Sint32)div>>1)) < (obj->param.x+obj->param.w)) 
   vline(SPACER+((Sint32)div>>2)+((Sint32)div>>1), obj->param.y+10, (obj->param.y+obj->param.h)-10, 
    &color, obj->param.bg, NO_HASH); 
 }

 snprintf(buf, MAX_CHARS, "%d%c",(Sint32)(globl_time/scale)+i,s);
 clip = strlen(buf)*8; 

 if(div<clip) return;

 for(i = 0;i<j;i++) {

  if(SPACER-CENTER_OF_STRING(buf) > obj->param.x+8 &&
     SPACER-CENTER_OF_STRING(buf) < obj->param.x + obj->param.w-clip) {
   k = (Sint32)(globl_time/scale)+i;
   snprintf(buf, MAX_CHARS, "%d%c",k,s);
   draw_text( SPACER - CENTER_OF_STRING(buf), obj->param.y+obj->param.h-9, buf,
     &color,obj->param.bg, NO_HASH, 0); 
  }
 }
#undef SPACER
}
/* }}} */
/* proc_time_bar {{{ */
Sint32 proc_time_bar(Sint32 msg, struct object_t *obj, Sint32 data) {
 SDL_Rect *dst;
 Sint32 i, *p2, j, k;
 double store, store2;
 Sint32 x, px, lpx, cur_x;
 unsigned char *p1;
 Sint32 div;
 double scaler;
 color_t color;
 switch(msg) {
  case MSG_START:
   obj->param.h = 30;
   obj->param.d1 = SDL_MapRGB(gui_screen->format, 
    obj->param.bg->r, obj->param.bg->g, obj->param.bg->b);
   obj->param.dp1=(void *)malloc(sizeof(SDL_Rect));
   dst = (SDL_Rect *)obj->param.dp1;
   dst->x = obj->param.x;
   dst->y = obj->param.y;
   dst->w = obj->param.w;
   dst->h = obj->param.h;
   break;
  case MSG_KEYDOWN:
   switch(data) {
    case SDLK_LEFT:
     globl_time-= 1.0f / (((double)obj->param.w*zoom)/5.0f);
     if(globl_time < 0.0f)
      globl_time = 0.0f;
     MESSAGE_OBJECT(obj, MSG_DRAW);
     break;
    case SDLK_RIGHT:
     globl_time+= 1.0f / (((double)obj->param.w*zoom)/5.0f);
     MESSAGE_OBJECT(obj, MSG_DRAW);
     break;
    case SDLK_UP:
     globl_time-= 5.0f/zoom;
     if(globl_time < 0.0f) 
      globl_time = 0.0f;
     MESSAGE_OBJECT(obj, MSG_DRAW);
     break;
   case SDLK_DOWN:
     globl_time+= 5.0f/zoom;
     MESSAGE_OBJECT(obj, MSG_DRAW);
     break;
   }
   break;
  case MSG_CLICK:
   x = gui_mouse_x;
   store = globl_time;
   store2 = zoom;
   for(;;) {
    cur_x = gui_mouse_x;
    if(lpx != px) 
     MESSAGE_OBJECT(obj, MSG_DRAW);
    lpx = px;
    if(wait_on_mouse()==MOUSE_UP) break;
    px = cur_x - x;
    k = SDL_GetModState();
    if(k!=j) {
     x = cur_x;
     store = globl_time;
    } 

    if((k & KMOD_SHIFT) !=0) {
     if(k!=j) {
      store2 = zoom;
#define OBJ_X (main_obj->param.x + tracks[0].wave->param.x + 20)
      scaler = (double)(cur_x - OBJ_X);
     }
     if(((double)(cur_x - OBJ_X) / scaler)>0.0f)
      zoom = store2 *((double)(cur_x - OBJ_X) / scaler) ;
#undef OBJ_X
    } else {
     globl_time=store- (double)px / (((double)obj->param.w*zoom)/5);
     if(globl_time < 0) {
      store = globl_time = 0;
      x = cur_x;
     }
    }
    j = k;
   }
   break;
  case MSG_DRAW:
   POP_TIMEBAR;
   SDL_FillRect(time_bar_bmp, obj->param.dp1, obj->param.d1);

   if(zoom <= 0.0000009f) {
    draw_text(obj->param.x + (obj->param.w/2) - CENTER_OF_STRING(TOO_LONG),
      obj->param.y + (obj->param.h/2) - 4, TOO_LONG, obj->param.fg, obj->param.bg, NO_HASH,0);
    goto too_long;
   }

   do_line(obj, 1.0f/512.0f, '~');
   do_line(obj, 1.0f, 's');
   do_line(obj, 60.0f,'m');
   do_line(obj, 3600.0f,'h');
   do_line(obj, 3600.0f*24.0f,'d');


   hline(obj->param.x, obj->param.y+(obj->param.h/2), obj->param.x+obj->param.w, 
   obj->param.fg, obj->param.bg, NO_HASH); 
too_long:
   PROT;
   SDL_UpdateRect(time_bar_bmp, 0,0,0,0);
   UNPROT;
   PUSH;
   POP_SUB;
   intern_update = sub_update;
   for(i = 0;i<TRACKS;i++) 
    if(tracks[i].empty == 0) 
     MESSAGE_OBJECT(tracks[i].wave, MSG_DRAW);
   PUSH;

   break;
 }
 return RET_OK;
}
/* }}} */
Sint32 proc_back(Sint32 msg, struct object_t *obj, Sint32 data) {
 const color_t color = { 100,100,100 };
 Sint32 q;
 unsigned char *p;
 if(msg == MSG_DRAW) {
  /* this draws the background image
   * if you want you can fill this with all
   * sort of fancy stuff, only gets called 
   * once- practically free. I like this 
   * xor fractal */
  p = (unsigned char *)gui_screen->pixels;
  for(q = 0;q<(gui_screen->h*gui_screen->pitch);q++) 
   *p++ = ((q%gui_screen->pitch)>>6)^((q/gui_screen->pitch)>>2);
  HOST_UNBLOCK;
 }

 return RET_OK;
}

Sint32 proc_progress_bar(Sint32 msg, struct object_t *obj, Sint32 data) {
 SDL_Surface *bmp;
 SDL_Rect src, dst;
 char buf[MAX_CHARS];
 float per;
 switch(msg) {
  case MSG_START:
   bmp = IMG_ReadXPMFromArray(obj->param.dp1);
   obj->param.dp1 = bmp;
   bmp = IMG_ReadXPMFromArray(obj->param.dp2);
   obj->param.dp2 = bmp;
   obj->param.d1 = 0;
   obj->param.d2 = PROGRESS_BAR_DETER;
   break;
  case MSG_DRAW:
   src.x = 0; src.y = 0; 
   src.w = dst.w = obj->param.w; src.h = dst.w = obj->param.h;
   dst.x = obj->param.x; dst.y = obj->param.y;
   SDL_BlitSurface(obj->param.dp1, &src, gui_screen, &dst);

   if(obj->param.d2 == PROGRESS_BAR_DETER) {
    if(obj->param.d1 == 0) {
     norm_update(obj);
     break;
    }
    per = (float)obj->param.d1 / 100.0f;
  
    src.w = (Sint32)(per * (float)obj->param.w); dst.w = src.w;
    SDL_BlitSurface(obj->param.dp2, &src, gui_screen, &dst);
    snprintf(buf, MAX_CHARS,"%d%%", obj->param.d1);
    draw_text(obj->param.x +(obj->param.w/2)- CENTER_OF_STRING(buf), 
              obj->param.y+(obj->param.h/2)-4,
              buf, obj->param.bg, obj->param.bg, NO_HASH, 0);
   } else {
    per = (float)(obj->param.d1%100) / 100.0f;
    src.x = (Sint32)(per * (float)(obj->param.w -20));
    if((obj->param.d1%200) >= 100)
     src.x = (obj->param.w - 20) - src.x;
    src.w = 20;
    dst.w = 20;
    dst.x = src.x + obj->param.x;
    SDL_BlitSurface(obj->param.dp2, &src, gui_screen, &dst);
    obj->param.d1++;
   }
   norm_update(obj);
   break;
 }
 return RET_OK;
}

char message_bar_null = 0;

Sint32 proc_message_bar(Sint32 msg, struct object_t *obj, Sint32 data) {
 SDL_Rect *dst;
 switch(msg) {
  case MSG_START:
   obj->param.dp2 = &message_bar_null;
   dst = obj->param.dp1 = (void *)malloc(sizeof(SDL_Rect));
   dst->x = obj->param.x;
   dst->y = obj->param.y;
   dst->w = obj->param.w;
   dst->h = obj->param.h;
   obj->param.d1 = SDL_MapRGB(gui_screen->format, obj->param.bg->r,
                                                  obj->param.bg->g,
						  obj->param.bg->g);
   break;
  case MSG_DRAW:
   SDL_FillRect(gui_screen, (SDL_Rect *)obj->param.dp1, obj->param.d1);
   draw_text(obj->param.x + (obj->param.w/2) - CENTER_OF_STRING(obj->param.dp2), obj->param.y+(obj->param.h/2)-4,
     obj->param.dp2, obj->param.fg, obj->param.bg, NO_HASH, 0);
   norm_update(obj);
   break;
 }
 return RET_OK;
}



Sint32 proc_memory_bar(Sint32 msg, struct object_t *obj, Sint32 data) {
 struct rusage r;
 SDL_Rect *dst;
 char buf[MAX_CHARS];
 char buf2[MAX_CHARS];
 switch(msg) {
  case MSG_START:
   dst = obj->param.dp1 = (void *)malloc(sizeof(SDL_Rect));
   dst->x = obj->param.x;
   dst->y = obj->param.y;
   dst->w = obj->param.w;
   dst->h = obj->param.h;
   obj->param.d1 = SDL_MapRGB( gui_screen->format, 
     obj->param.bg->r, obj->param.bg->g, obj->param.bg->b);
   break;
  case MSG_DRAW:
   dst = (SDL_Rect *)obj->param.dp1;
   SDL_FillRect( gui_screen, dst, obj->param.d1);
   if(getrusage(RUSAGE_SELF, &r)<0) break;
   /* XXX how do we get drss?! */
   snprintf(buf, MAX_CHARS, "%d", r.ru_idrss);
   snprintf(buf2, MAX_CHARS, "%d", r.ru_inblock+r.ru_oublock);


/*
   printf("%d %d %10ld %d %d %d %d\n"
          "%d %d %d %d %d %d %d\n", 
	  (int)r.ru_maxrss,
	  (int)r.ru_ixrss,
	  r.ru_idrss,
	  (int)r.ru_isrss,
	  r.ru_minflt,
	  r.ru_majflt,
	  r.ru_nswap,
	  r.ru_inblock,
	  r.ru_oublock,
	  r.ru_msgsnd,
	  r.ru_msgrcv,
	  r.ru_nsignals,
	  r.ru_nvcsw,
	  r.ru_nivcsw);
*/

   draw_text(obj->param.x+((obj->param.w/2)-CENTER_OF_STRING(buf)), 
     obj->param.y+1,
     buf, obj->param.fg, obj->param.bg, NO_HASH, 0);

   draw_text(obj->param.x+((obj->param.w/2)-CENTER_OF_STRING(buf2)), 
     (obj->param.y+obj->param.h)-9,
     buf2, obj->param.fg, obj->param.bg, NO_HASH, 0);

   UPDATE_OBJECT(obj);
  break;
 }
 return RET_OK;
}

Sint32 proc_end_splash(Sint32 msg, struct object_t *obj, Sint32 data) {
 switch(msg) {
  case MSG_MOUSEMOVE:
  case MSG_KEYUP:
  case MSG_KEYDOWN:
   return RET_QUIT;
 }
 return RET_OK;
}

Sint32 proc_start_splash(Sint32 msg, struct object_t *obj, Sint32 data) {
 if(msg == MSG_AFTERDRAW) 
  group_loop(splash_grp);
 return RET_OK;
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgxAACgkQMNO4A6bnBrOELQCfRW5C+6jwebv/z7V91Xf99p8S
5owAn0Opk8nCp3Iq4slrqeGO8GjjSuxq
=Jw9d
-----END PGP SIGNATURE-----
*/
