/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "includes.h"

void player_recv(int msg, void *data, int len) {
 printf("you are falling asleep\n");
}

void coder_recv(int msg, void *data, int len) {
 switch(msg) {
  case NOT_CUE:
   printf("pants cue\n");
   input_file(data);
   CODER_RETURN(1337);
   break;
 }
}

void halt(void){
 for(;;) 
  SDL_Delay(3000);
}

int player_main(void) {
 init_not(PLAYER, player_recv);
 halt();
}


int coder_main(void) {
 int number;

 init_not(CODER, coder_recv);
 send_host(NOT_SYNC, NULL, 0, STORE);
 send_host(NOT_MESSAGE_BAR, "Initializing audio...", 0, STORE);
// avcodec_init();
// avcodec_register_all();
// av_register_all();
 send_host(NOT_MESSAGE_BAR, "", 0, STORE);

 for(;;number++) {
  number%=100;
  send_host(NOT_PROGRESS_BAR, &number, 0, DROP);
  SDL_Delay(10);
 }

 coder_init();
// coder_run();

// au_load("/tmp/test.mp3");
 halt();
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvkACgkQMNO4A6bnBrOdgQCgiELSy47rDUmZ4ApijU7N3EQO
+7MAn3FJA4lYugt4sN6Ur09rU2hX586p
=r45d
-----END PGP SIGNATURE-----
*/
