/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "includes.h"

char *av_error_string(Sint32 in) {
 char *ret;
 switch(in) {
  case AVERROR_UNKNOWN:
   ret = "unknown error";
   break; 
  case AVERROR_IO:
   ret = "io error";
   break;
  case AVERROR_NUMEXPECTED:
   ret = "number syntax error";
   break;
/*  case AVERROR_INVALIDDATA:
   ret = "invalid data";
   break; */
  case AVERROR_NOMEM:
   ret = "not enough memory";
   break;
  case AVERROR_NOFMT:
   ret = "unknown format";
   break;
  case AVERROR_NOTSUPP:
   ret = "operation not supported";
   break;
  case AVERROR_NOENT:
   ret = "no such file or directory";
   break;
 }
 return ret;
}

void report(char *a, char *b) {
 char buf[MAX_CHARS];
 snprintf(buf, MAX_CHARS, "%s: %s", a, b);
 send_host(NOT_MESSAGE_BAR, buf, 0, STORE );
}

/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvMACgkQMNO4A6bnBrPCTQCfXc7UvP+NEjZXc5XrYNF6GZaj
Z5QAoJA8Gk7Zu+gyLJWQu1MhppcwBWts
=w6xl
-----END PGP SIGNATURE-----
*/
