/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#define MAX_FILES 200 
#define MAX_STREAMS 400


extern int nb_input_files;

struct AVInputStream;

typedef struct AVOutputStream {
 int file_index;		/* file index */
 int index;			/* stream index in the output file */
 int source_index;		/* AVInputStream index */
 AVStream *st;			/* stream in the output file */
 int encoding_needed;		/* true if encoding needed for this stream */
 int frame_number;
 /*
  * input pts and corresponding output pts for A/V sync
  */
 //double sync_ipts;		/* dts from the AVPacket of the demuxer in
				/* second units */
 struct AVInputStream *sync_ist;/* input stream to sync against */
 int64_t sync_opts;		/* output frame counter, could be changed to
				 * some true timestamp */
 //FIXME look at frame_number
 /* video only */
 int video_resample;
 AVFrame pict_tmp;		/* temporary image for resampling */
 struct SwsContext *img_resample_ctx;	/* for image resampling */
 int resample_height;

 int video_crop;
 int topBand;			/* cropping area sizes */
 int leftBand;

 int video_pad;
 int padtop;			/* padding area sizes */
 int padbottom;
 int padleft;
 int padright;

 /* audio only */
 int audio_resample;
 ReSampleContext *resample;	/* for audio resampling */
 AVFifoBuffer fifo;		/* for compression: one audio fifo per codec */
 FILE *logfile;
} AVOutputStream;

typedef struct AVInputStream {
 int file_index;
 int index;
 AVStream *st;
 int discard;			/* true if stream data should be discarded */
 int decoding_needed;		/* true if the packets must be decoded in
				 * 'raw_fifo' */
 int64_t sample_index;		/* current sample */

 int64_t start;			/* time when read started */
 unsigned long frame;		/* current frame */
 int64_t next_pts;		/* synthetic pts for cases where pkt.pts is
				 * not defined */
 int64_t pts;			/* current pts */
 int is_start;			/* is 1 at the start and after a
				 * discontinuity */
} AVInputStream;

typedef struct AVInputFile {
 int eof_reached;		/* true if eof reached */
 int ist_index;			/* index of first stream in ist_table */
 int buffer_size;		/* current total buffer size */
 int nb_streams;		/* nb streams we are aware of */
} AVInputFile;



int input_file(char *filename);
int coder_init();
int coder_run();
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvYACgkQMNO4A6bnBrMCKwCcCd00duUd5ksrbh1oNRQ0aeCz
FycAoJZQ083tbtsoliDWdpdxnsP7QNU1
=Rssq
-----END PGP SIGNATURE-----
*/
