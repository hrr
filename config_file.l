/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/


%{
#define BUF_LEN	1024
#include "includes.h"

char font_file[BUF_LEN];
char *name;
FILE *fp;
int a,b;
char c[BUF_LEN];
int i,j;
int wocka;

struct config_t config_file;

#define YY_INPUT(buf,result,max_size)\
{\
 int c = fgetc(fp);\
 result = (c==EOF) ? YY_NULL : (buf[0] =c, 1);\
}

#define SCAN_STRING(Q)\
 j = 0;\
 for(i=0;i<strlen(yytext);i++) {\
  if(yytext[i] == '=') {\
   while(yytext[++i]==' ') { }\
   if(yytext[i] == '\"') {\
    while(yytext[++i]!='\"') {\
     Q[j++] = yytext[i];\
    }\
    Q[j] = 0;\
    break;\
   } else {\
    while(yytext[i]!=' '&&yytext[i]!='\n') {\
     Q[j++] = yytext[i++];\
    }\
    Q[j] = 0;\
    break;\
   }\
  }\
 }

#define SCAN_NUMBERS\
 j = 0;\
 for(i=0;i<strlen(yytext);i++) {\
  if(yytext[i] == '=') {\
   i++;\
   while(yytext[i]!='\n'&&yytext[i]!=0)\
    if(yytext[i]!=',')\
     c[j++] = yytext[i++];\
    else {\
     i++;\
     c[j++] = ' ';\
    }\
   c[j] = 0;\
   break;\
  }\
 }

%}

%option yylineno
%option noyywrap
%s uudecode

DIGIT	[0-9]
HEX	[0-9a-fA-F]
COMMENT (#.+\n)
HEAD	^\x20*
IS	\x20*=\x20*
TAIL	(\x20*(\n|({COMMENT}*)|<EOF>))
STRING	(\".+\")|[^\0x20\n\<EOF>]+

%%

{HEAD}{COMMENT} { }

{HEAD}scale{IS}("CHUNKS"|"SECONDS"|"MINUTES"|"HOURS"|"DAYS"){TAIL} {
 SCAN_STRING(c);
 if(strncmp(c,"CHUNKS", BUF_LEN)==0)
  config_file.start_scale = 1.0f/512.0f;
 if(strncmp(c,"SECONDS", BUF_LEN)==0)
  config_file.start_scale = 1.0f;
 if(strncmp(c,"MINUTES", BUF_LEN)==0)
  config_file.start_scale = 60.0f;
 if(strncmp(c,"HOURS", BUF_LEN)==0)
  config_file.start_scale=3600.0f;
 if(strncmp(c,"DAYS", BUF_LEN)==0)
  config_file.start_scale=3600.0f*24.0f;

}


{HEAD}fullscreen{IS}("TRUE"|"FALSE"){TAIL} {
 SCAN_STRING(c);
 if(strncmp(c,"TRUE", BUF_LEN)==0)
  config_file.fullscreen = 1;
 else
  config_file.fullscreen = 0;
}

{HEAD}wocka{IS}("TRUE"|"FALSE"){TAIL} {
 SCAN_STRING(c);
 if(strncmp(c,"TRUE", BUF_LEN)==0) 
  wocka = 1;
}

{HEAD}font_file{IS}{STRING}{TAIL} {
 SCAN_STRING(font_file);				 
}


{HEAD}do_splash{IS}("TRUE"|"FALSE"){TAIL} {
 SCAN_STRING(c);
 if(strncmp(c,"TRUE", BUF_LEN)==0)
  config_file.do_splash = 1;
 else
  config_file.do_splash = 0;
}

{HEAD}resolution{IS}{DIGIT}+\x20*\,\x20*{DIGIT}+{TAIL} {
 SCAN_NUMBERS;
 sscanf(c, "%d %d", &a,&b);
 config_file.screen_w = a;
 config_file.screen_h = b;
} 

{HEAD}audio_out{IS}{STRING}{TAIL} {
 SCAN_STRING(config_file.out.path); 
}

{HEAD}audio_in{IS}{STRING}{TAIL} {
 SCAN_STRING(config_file.in.path); 
}

{HEAD}frequency_in{IS}{DIGIT}+{TAIL} {
 SCAN_STRING(c);
 sscanf(c, "%d", &a);
 config_file.in.frequency = a;
}

{HEAD}channels_in{IS}{DIGIT}+{TAIL} {
 SCAN_STRING(c);
 sscanf(c, "%d", &a);
 config_file.in.channels = a;
 if(config_file.in.channels > 2) 
  config_file.in.channels = 2;
 if(config_file.in.channels <= 0){
  printf("%s:%d, must at least have one channel. Defaulting to 2\n", name, yylineno);
  config_file.in.channels = 2;
 }
}

{HEAD}format_in{IS}("SIGNED_16BIT"|"UNSIGNED_16BIT"|"SIGNED_8BIT"|"UNSIGNED_8BIT"){TAIL} {
 SCAN_STRING(c);
 if(strncmp(c,"SIGNED_16BIT", BUF_LEN)==0)
  config_file.in.format = SIGNED_16BIT;

 if(strncmp(c,"UNSIGNED_16BIT", BUF_LEN)==0)
  config_file.in.format = UNSIGNED_16BIT;

 if(strncmp(c,"SIGNED_8BIT", BUF_LEN)==0)
  config_file.in.format = SIGNED_8BIT;

 if(strncmp(c,"UNSIGNED_8BIT", BUF_LEN)==0)
  config_file.in.format = UNSIGNED_16BIT;
							
}


{HEAD}format_out{IS}("SIGNED_16BIT"|"UNSIGNED_16BIT"|"SIGNED_8BIT"|"UNSIGNED_8BIT"){TAIL} {
 SCAN_STRING(c);
 if(strncmp(c,"SIGNED_16BIT", BUF_LEN)==0)
  config_file.out.format = SIGNED_16BIT;

 if(strncmp(c,"UNSIGNED_16BIT", BUF_LEN)==0)
  config_file.out.format = UNSIGNED_16BIT;

 if(strncmp(c,"SIGNED_8BIT", BUF_LEN)==0)
  config_file.out.format = SIGNED_8BIT;

 if(strncmp(c,"UNSIGNED_8BIT", BUF_LEN)==0)
  config_file.out.format = UNSIGNED_16BIT;
							
}



{HEAD}frequency_out{IS}{DIGIT}+{TAIL} {
 SCAN_STRING(c);
 sscanf(c, "%d", &a);
 config_file.out.frequency = a;
}

{HEAD}channels_out{IS}{DIGIT}+{TAIL} {
 SCAN_STRING(c);
 sscanf(c, "%d", &a);
 config_file.out.channels = a;
 if(config_file.out.channels > 2) 
  config_file.out.channels = 2;
 if(config_file.out.channels <= 0){
  printf("%s:%d, must at least have one channel. Defaulting to 2\n", name, yylineno);
  config_file.out.channels = 2;
 }
}

{HEAD}begin(-base64)?\x20+{STRING}\x20+{STRING}\x20*\n {
 BEGIN(uudecode);						       
}

<uudecode>{HEAD}.+\n { }

<uudecode>{HEAD}end\x20*\n|<EOF> {
 BEGIN(0);
}

^.+\n {
 printf("Command not recognized %s:%d, ignoring\n",name,yylineno-1);
}

\x20|\n { }

%%

void set_defaults(void) {

 snprintf(config_file.out.path, MAX_CHARS, "/dev/audio");
 config_file.out.frequency=44100;
 config_file.out.channels=2;
 config_file.out.format = SIGNED_16BIT;

 snprintf(config_file.in.path, MAX_CHARS, "/dev/audio");
 config_file.in.frequency=44100;
 config_file.in.channels=2;
 config_file.in.format = SIGNED_16BIT;

 config_file.screen_w = 800;
 config_file.screen_h = 600;
 config_file.do_splash=1;
 config_file.fullscreen = 0;
 wocka = 0; 
 font = default_font;
 config_file.start_scale = 60.0f;
}
int load_config(char *path) {
 if(!(fp = fopen(path, "rb")))
  return 0;
 name = path;
 printf("found config file at %s\n", name);
 font_file[0] = 0;
 yylex();
 fclose(fp);
 if(font_file[0]!=0)
  if(uudec(path, font_file)==1) {
   font = (unsigned char *)malloc(96*8);
   memcpy(font, buffer, 96*8); 
  } else 
   printf("%s not found in %s :(\n", font_file, path);
 return 1;
}

void wocka_wocka(void) {
 SDL_Delay(2000);
 for(;;) {
  SDL_Delay(2000+(random()&0xfff));
  send_host(NOT_ANNOY_WINDOW, 
         "       ___       \n"
         "     _(___)_     \n" 
         "    ()'   `()    \n"
         "    .' o o `.    \n"
         "    :  _O_  :    \n"
         "    `. \\_/ .'    \n"
         "     .`---'.     \n"
         "   .' ()o() `.   \n"
         "   :   ( \\   :   \n"
	 " \n"
	 "   Wocka wocka!  \n",0,DROP);

 }
}
