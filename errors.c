#include <stdio.h>

#define MAX_IDENT	256
#define CREATE	0
#define FIND	1


int main(int argc, char **argv) {
 int mode;
 int cap = 0;
 int phase = 0;
 int inside = 0;
 int this_is_it = 0;
 char ident[MAX_IDENT];
 FILE *db;
 char dat = 0, prev;
 if(argc<2)  {
  printf("usage: ./errors <error_ident>|<create>\n");
  exit(-1);
 }

 if(strncmp( argv[1], "create", strlen("create"))==0) 
  mode = CREATE;
 else
  mode = FIND;

 if((db = fopen("errors.db", "rb")) <=0) {
  perror("errors.db");
  exit(-1);
 }

 if(mode == CREATE) 
  puts("#include <stdarg.h>\n"
       "\n"
       "char not_found[0xff];\n"
       "\n"
       "typedef struct {\n"
       " char *ident, *body, *vars;\n"
       "} error_t;\n"
       "\n"
       "error_t errors[] = {");

#define PULL \
 prev = dat; \
 if((dat= fgetc(db))<0) \
  if(feof(db)!=0) goto done;

 for(;;) {
  PULL;
  if( dat == '^' && prev != '\\') {
   inside^=1;
   if(inside == 0) {
    phase++; 
    phase%=3;
    if(mode == FIND) {
     if(this_is_it == 1) break;
     if((cap>0) && phase == 1) { 
      ident[cap] = 0;
      if(strncmp(ident, argv[1], MAX_IDENT)==0)
       this_is_it = 1;
     }
     cap = 0;
    } else  
     // CLOSE
     switch(phase) {
      case 0:
       puts("\"},");
       break;
      case 1:
      case 2:
       printf("\"");
       break;
     }
   } else
    if(mode == CREATE) 
     // OPEN
     switch(phase) {
      case 0:
       printf(" {\"");
       break;
      case 1:
      case 2:
       printf(", \"");

       break;
     }

  } else
   if(inside == 1) {
    if(mode == FIND) {
     if(this_is_it == 0) {
      ident[cap++] = dat;
      if(cap>=MAX_IDENT-1) cap = 0;
     }  else 
      putchar(dat);
    } else 
     switch(dat) {
      case '\n':
       printf("\\n");
       break;
      case '\t':
       printf("\\t");
       break;
      case '\r':
       printf("\\r");
       break;
      default:
       putchar(dat);
       break;
     }
   }
 } 

done:
 if(mode == CREATE) 
  puts(
  " { (char *)0, (char *)0, (char *)0} \n"
  "}; \n"
  "\n"
  "char *get_error(char *ident, ... ) { \n"
  " int i; \n"
  " char *body, *vars; \n"
  " for(i=0;;i++) { \n"
  "  if(errors[i].ident == 0) break; \n"
  "  if(strncmp( errors[i].ident, ident, strlen(ident))==0) break; \n"
  " } \n"
  " if(errors[i].ident == 0) {\n"
  "  snprintf(not_found, 0xff, \n"
  "    \"There is no error-text for this in errors.db, I suggest you put something there.\\n\"\n"
  "    \"The ident string is \\\"%s\\\"\", ident);\n"
  "  return not_found;\n"
  " }\n"
  " return errors[i].body;\n"
  "}"
  );
 else 
  if(this_is_it == 0) 
   printf("There is no error-text for this in errors.db, I suggest you put something in there.\n"
          "The ident string is \"%s\"\n", argv[1]);
}
