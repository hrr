#!/bin/sh

#  m m mm mmm .----------.  .---------------------. mmm mm m m
#  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
#  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
#  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
#  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
#  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
#                ==============`            `---`
#                                 L A B O R A T O R I E S
#  
#This file is part of Hacker Radio Rec.
#  
#Hacker Radio Rec is free software: you can redistribute it and/or
#modify it under the terms of the GNU General Public License as
#published by the Free Software Foundation, either version 3 of
#the License or (at your option) any later version.
#
#Hacker Radio Rec is distributed in the hope that it will be
#useful, but WITHOUT ANY WARRANTY; without even the implied
#warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#
#               Copyright (C) 2009, Thea DeSilva
#  You can find a copy of GNU General Public License in COPYING
#



export AUTOCONF_VERSION=2.61
export AUTOMAKE_VERSION=1.4

make -i clean

rm -rf gui/Makefile not/Makefile audio/Makefile Makefile config.h autom4te.cache config.log config.status aclocal.m4
aclocal && autoconf
