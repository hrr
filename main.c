/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "includes.h"
#include "pictures.h"

struct menu_entry_t menu_wave_text[] = {
 {"copy",   CALL_BUTTON, MENU_WAVE_COPY,   menu_bottom},
 {"cut",    CALL_BUTTON, MENU_WAVE_CUT,    menu_bottom},
 {"paste",  CALL_BUTTON, MENU_WAVE_PASTE,  menu_bottom},
 {"zoom to",CALL_BUTTON, MENU_WAVE_ZOOMTO, menu_bottom},
 {"clear",  CALL_BUTTON, MENU_WAVE_CLEAR,  menu_bottom},
 {"delete", CALL_BUTTON, MENU_WAVE_DELETE, menu_bottom},
 {NULL, NULL, NULL, NULL}
};

const color_t menu_color = { 0xaf, 0xaf, 0x54 };

struct menu_entry_t menu_button_text[] = {
 {"load", CALL_BUTTON, MENU_LOAD, menu_bottom},
 {"save", CALL_BUTTON, MENU_SAVE, menu_bottom},
 {"clear selection", CALL_BUTTON, MENU_CLEAR, menu_bottom},
 {"clear track", CALL_BUTTON, MENU_CLEAR_TRACK, menu_bottom},
 {NULL, NULL, NULL, NULL}
};

struct select_file_t *cue_file;

stk_t stk[STACK_SIZE];
Sint32 stk_pointer = 0;

char tmp_path[MAX_CHARS] = { 0 };

group_t *main_grp, *sub_grp, *splash_grp, *time_bar_grp, *menu_grp, *menu_wave_grp;

struct object_t *vert, *time_bar, *main_obj;

struct object_t *start_obj, *stop_obj, *pause_obj, *play_obj, *end_obj, *rec_obj;
struct object_t *progress_bar, *message_bar, *memory_bar;

/* clear_dir {{{ */

Sint32 clear_dir_depth;

Sint32 clear_dir(char *path) {
 struct stat sb, qsb;
 Sint32 nbytes;
 char buf2[MAX_CHARS];
 long base;
 struct dirent *dp;
 unsigned char *buf, *ebuf, *cp;
 int fp;
 if((fp =open(path, O_RDONLY,0)) == -1)
  return;
 fstat(fp, &sb);
 buf = (unsigned char *)malloc(sb.st_size);

 clear_dir_depth++;
 while((nbytes= getdirentries(fp,buf,sb.st_size,&base)) > 0 ){
  ebuf = buf+nbytes;
  cp = buf;
  while(cp<ebuf) {
   dp = (struct dirent *)cp;
   if(dp->d_fileno!=0) {
    if(dp->d_type == DT_REG) {
     snprintf(buf2, MAX_CHARS, "%s/%s", path, dp->d_name);
     if(unlink(buf2)<0) return 0;
    }
    if(dp->d_type == DT_DIR &&
       strcmp(".", dp->d_name)!=0 &&
       strcmp("..",dp->d_name)!=0) {
     snprintf(buf2,MAX_CHARS-1,"%s/%s", path,dp->d_name);
     if(clear_dir(buf2)==0) return 0;
    }
   }
   cp+=dp->d_reclen;
  }
 }
 free(buf);
 close(fp);
 rmdir(path);
 putchar('.');
 fflush(stdout);
 clear_dir_depth--;
 if(clear_dir_depth == 0)
  putchar('\n');
 return 1;
}
/* }}} */
/* setup_windows {{{ */
void setup_windows(Sint32 w, Sint32 h) {
 obj_param_t tmp_parm;
 struct object_t *tmp_obj;

 main_grp = new_group(0,0, gui_screen->w, gui_screen->h, 0,0);

// PARM(0,0, w, h, &globl_fg, &globl_bg,  0, proc_back); 

 tmp_parm.x = 0; tmp_parm.y = 0;
 tmp_parm.w = w; tmp_parm.h = h;
 tmp_parm.fg = &globl_fg;
 tmp_parm.bg = &globl_bg;
 tmp_parm.flags = 0;
 tmp_parm.proc = proc_back;

 new_obj(main_grp, &tmp_parm);

 if(config_file.do_splash==1) {
  tmp_parm.proc = proc_start_splash;
  new_obj(main_grp, &tmp_parm);
 }


 PARM( VX(311)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)move_xpm;
 tmp_parm.d1 = FALSE;
 tmp_parm.d2 = 1;
 tmp_parm.callback = bottom;
 tmp_parm.user_flags = CMD_MOVE;
 new_obj(main_grp, &tmp_parm);


 PARM( VX(311), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(275)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)envelope_xpm;
 tmp_parm.d2 = 1;
 tmp_parm.d1 = FALSE;
 tmp_parm.callback = bottom;
 tmp_parm.user_flags = CMD_ENVELOPE;
 new_obj(main_grp, &tmp_parm);


 PARM( VX(275), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(237)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)select_xpm;
 tmp_parm.d2 = 1;
 tmp_parm.d1 = TRUE;
 tmp_parm.callback = bottom;
 tmp_parm.user_flags = CMD_SELECT;
 new_obj(main_grp, &tmp_parm);
 current_tool = TOOL_SELECT;
 PARM( VX(237), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);




 PARM( VX(94), VY(8), VX(85), VY(22), &globl_fg, &globl_bg, DROP_SHADOW|SHOW_FOCUS|CALL_BUTTON, proc_button_box);
 tmp_parm.dp1 = (void *)"menu";
 tmp_parm.user_flags = CMD_MENU;
 tmp_parm.callback = bottom;
 tmp_obj = new_obj(main_grp, &tmp_parm);

 PARM( VX(6), VY(8), VX(85), VY(22), &globl_fg, &globl_bg, DROP_SHADOW|SHOW_FOCUS|CALL_BUTTON, proc_button_box);
 tmp_parm.dp1 = (void *)"live";
 tmp_parm.callback = bottom;
 tmp_parm.user_flags = CMD_LIVE; 
 new_obj(main_grp, &tmp_parm); 

 PARM( VX(707), VY(8), VX(85), VY(22), &globl_fg, &globl_bg, DROP_SHADOW|SHOW_FOCUS|CALL_BUTTON, proc_button_box);
 tmp_parm.dp1 = (void *)"quit";
 tmp_parm.callback = nah_really;
 new_obj(main_grp, &tmp_parm);

 PARM( VX(619), VY(8), VX(85), VY(22), &globl_fg, &globl_bg, DROP_SHADOW|SHOW_FOCUS|CALL_BUTTON, proc_button_box);
 tmp_parm.dp1 = (void *)"manual";
 tmp_parm.callback = bottom;
 tmp_parm.user_flags = CMD_MANUAL;
 new_obj(main_grp, &tmp_parm);



 PARM( VX(575)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, 
   LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|SINGLE_RADIO|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)end_xpm;
 tmp_parm.d2 = 2;
 tmp_parm.d1 = FALSE;
 tmp_parm.user_flags = CMD_END;
 tmp_parm.callback = bottom;
 end_obj = new_obj(main_grp, &tmp_parm);
 PARM( VX(575), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(539)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, 
   LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|SINGLE_RADIO|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)rec_xpm;
 tmp_parm.d2 = 2;
 tmp_parm.d1 = FALSE;
 tmp_parm.user_flags = CMD_REC;
 tmp_parm.callback = bottom;
 rec_obj = new_obj(main_grp, &tmp_parm);
 PARM( VX(539), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(503)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, 
   LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|SINGLE_RADIO|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)play_xpm;
 tmp_parm.d2 = 2;
 tmp_parm.d1 = FALSE;
 tmp_parm.user_flags = CMD_PLAY;
 tmp_parm.callback = bottom;

 play_obj = new_obj(main_grp, &tmp_parm);
 PARM( VX(503), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(467)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, 
   LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|SINGLE_RADIO|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)pause_xpm;
 tmp_parm.d2 = 2;
 tmp_parm.d1 = FALSE;
 tmp_parm.user_flags = CMD_PAUSE;
 tmp_parm.callback = bottom;
 pause_obj = new_obj(main_grp, &tmp_parm);
 PARM( VX(467), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);
 
 PARM( VX(431)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg, 
   LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|SINGLE_RADIO|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)stop_xpm;
 tmp_parm.d2 = 2;
 tmp_parm.d1 = FALSE;
 tmp_parm.user_flags = CMD_STOP;
 tmp_parm.callback = bottom;
 stop_obj = new_obj(main_grp, &tmp_parm);
 PARM( VX(431), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(395)+1, VY(8)+1, 22, 20, &globl_fg, &globl_bg,
   LOAD_XPM_FROM_ARRAY|SHOW_FOCUS|SINGLE_RADIO|CALL_BUTTON, proc_icon_button);
 tmp_parm.dp1 = (void *)start_xpm;
 tmp_parm.d2 = 2;
 tmp_parm.d1 = FALSE;
 tmp_parm.user_flags = CMD_START;
 tmp_parm.callback = bottom;
 start_obj = new_obj(main_grp, &tmp_parm);
 PARM( VX(395), VY(8), 24, 22, &globl_fg, &globl_bg, DROP_SHADOW, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(778), VY(41), VX(11), VY(554)-35, &globl_fg, &globl_bg, SHOW_FOCUS|CALL_BUTTON, proc_scroll_bar);
 tmp_parm.d1 = 0;
 tmp_parm.d2 = 0xff; 
 tmp_parm.user_flags = CMD_VSCROLL;
 tmp_parm.callback = bottom;
 vert = new_obj(main_grp, &tmp_parm);


 PARM( VX(10), VY(41), VX(765), VY(554)-34, &globl_fg, &globl_bg, 0, proc_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(10)+1, VY(41)+1, VX(765)-1, VY(554)-35, &globl_fg, &globl_bg, 0, proc_main);
 main_obj = new_obj(main_grp, &tmp_parm);

 PARM(VX(10)+1,VY(554)+11, 127, 22, &globl_fg, &globl_bg, 0, proc_progress_bar);
 tmp_parm.dp1 = progress_bar_xpm;
 tmp_parm.dp2 = progress_bar2_xpm;
 progress_bar = new_obj(main_grp, &tmp_parm);

 PARM( VX(10), VY(554)+10, 128, 23, &globl_fg, &globl_bg, 0, proc_box);
 new_obj(main_grp, &tmp_parm);

 PARM( VX(10)+131, VY(554)+11, VX(786)-141-50, 22, &globl_bg, &globl_fg, 0, proc_message_bar);
 message_bar = new_obj(main_grp, &tmp_parm);


 PARM(message_bar->param.x+message_bar->param.w,
      message_bar->param.y, 50, 22, &globl_fg, &globl_bg, 0,
   proc_memory_bar);
 memory_bar = new_obj(main_grp, &tmp_parm);



 PARM( VX(10)+130, VY(554)+10, VX(786)-140, 23, &globl_fg, &globl_bg, 0, proc_box);
 new_obj(main_grp, &tmp_parm);
 
 
 PARM( VX(6), VY(37), VX(786), VY(554), &globl_fg, &globl_bg, 0, proc_shadow_box);
 new_obj(main_grp, &tmp_parm);



 menu_grp = 
  new_menu(tmp_obj->param.x+2, tmp_obj->param.y+2, menu_button_text, &globl_fg, &globl_bg); 

 menu_wave_grp = 
  new_menu(tmp_obj->param.x+2, tmp_obj->param.y+2, menu_wave_text, &globl_fg, &menu_color); 



 cue_file = setup_overlay_window(400, 300, LOAD, "pants", cue_file_bot); 
 
}
/* }}} */
/* setup_main_obj {{{ */
Sint32 setup_main_obj(void) {
 obj_param_t tmp_parm;
 Sint32 y;
 sub_grp = new_group(0,0, main_obj->param.w, main_obj->param.h,0,0);
 intern_update= norm_update;

 for(y=0;y<TRACKS;y++) {
  PARM( 6, 62 +(TRACK_SIZE * y) , 0, 8, &globl_fg, &globl_bg, SHOW_FOCUS | SINGLE_RADIO, proc_radio_button);
  tmp_parm.d1 = FALSE;
  tmp_parm.d2 = 3;
  tmp_parm.dp1 = (void *)"auto";
  new_obj(sub_grp, &tmp_parm);

  PARM( 6, 75 +(TRACK_SIZE *y), 0, 8, &globl_fg, &globl_bg, SHOW_FOCUS | SINGLE_RADIO, proc_radio_button);
  tmp_parm.d1 = FALSE;
  tmp_parm.d2 = 3;
  tmp_parm.dp1 = (void *)"mute";
  new_obj(sub_grp, &tmp_parm);

  PARM( 15, 90 +(TRACK_SIZE *y), 0, 0, &globl_fg, &globl_bg, 
    SHOW_FOCUS | SINGLE_RADIO |LOAD_XPM_FROM_ARRAY|CALL_BUTTON|FADE, proc_icon_button);
  tmp_parm.d1 = FALSE;
  tmp_parm.d2 = y;
  tmp_parm.dp1 = (void *)cue_xpm;
  tmp_parm.dp2 = (void *)cue_inactive_xpm;
  tmp_parm.callback = bottom;
  tmp_parm.user_flags = CMD_CUE;
  tracks[y].cue = new_obj(sub_grp, &tmp_parm);


  PARM( 11, 8 +TRACK_SIZE * y, 0,0, &globl_fg, &globl_bg, ((y&1)==0 ? BLUE : 0), proc_knob);
  tmp_parm.d1 = 0x80;
  tmp_parm.d2 = 0x100;
  new_obj(sub_grp, &tmp_parm); 

  PARM( 4, 2 + (TRACK_SIZE * y), 56, 110, &globl_fg, &globl_bg, 0, proc_shadow_box);
  new_obj(sub_grp, &tmp_parm);

  PARM( 66, 3 + (TRACK_SIZE * y), main_obj->param.w - 71, 169, &globl_fg, &globl_bg, 0, proc_wave);
  tmp_parm.d2 = y;
  tracks[y].wave = new_obj(sub_grp, &tmp_parm);
  tracks[y].dirty = 1;
  tracks[y].empty = 0;

  PARM( 65, 2 + (TRACK_SIZE * y), main_obj->param.w - 70, 170, &globl_fg, &globl_bg, 0, proc_shadow_box);
  new_obj(sub_grp, &tmp_parm);

 }

 tracks[0].empty = 0;

 left = IMG_ReadXPMFromArray(move_left_xpm);
 right= IMG_ReadXPMFromArray(move_right_xpm);

 printf("0x%x\n", left);
}
/*}}}*/
/* setup_splash {{{ */
void setup_splash(void) {
 obj_param_t tmp_parm; 
#define SPLASH_WIDTH	585
#define SPLASH_HEIGHT	400
 splash_grp = new_group( (gui_screen->w/2) - (SPLASH_WIDTH/2),
                         (gui_screen->h/2) - (SPLASH_HEIGHT/2),
			  SPLASH_WIDTH+2, SPLASH_HEIGHT+2, 0,0);
 simple_window(splash_grp, SPLASH_WIDTH, SPLASH_HEIGHT);
 PARM(3,15,0,0,&globl_fg, &globl_bg, LOAD_XPM_FROM_ARRAY, proc_bitmap);
 tmp_parm.dp1 = (void *)splash_xpm;
 new_obj(splash_grp, &tmp_parm);
 PARM(3,15,580,382,&globl_fg, &globl_bg, 0, proc_end_splash);
 new_obj(splash_grp, &tmp_parm);

#undef SPLASH_WIDTH
#undef SPLASH_HEIGHT
}
/* }}} */
/* setup_time_bar {{{ */
void setup_time_bar(void) {
 obj_param_t tmp_parm;
 time_bar_bmp = SDL_CreateRGBSurface(SDL_SWSURFACE, VX(766)-112, 30, 24, 
  gui_screen->format->Rmask, gui_screen->format->Gmask,
  gui_screen->format->Bmask, gui_screen->format->Amask); 
 if(time_bar_bmp <= 0) {
  printf("bunko\n");
  exit(-1);
 }
 time_bar_grp = new_group(0,0, time_bar_bmp->w, time_bar_bmp->h, 0, 0);

 PARM( 0, 0, time_bar_bmp->w, time_bar_bmp->h, &globl_bg, &globl_fg, SHOW_FOCUS|CALL_BUTTON, proc_time_bar);
 time_bar = new_obj(time_bar_grp, &tmp_parm);

 broadcast_group(time_bar_grp, MSG_START,0);
}
/* }}} */

void terminate(int sigraised, siginfo_t *info, void *a) {
 if(tmp_path[0] != 0) {
  printf("\nclearing %s", tmp_path); fflush(stdout);
  clear_dir_depth = 0;
  if(clear_dir(tmp_path)==0)
   printf("Yeah can't do that, you do it\n");
 }
 exit(-1);
}

SDL_Surface *ctx_old_gc, *time_bar_bmp;
group_t *ctx_old;
void *ctx_updater;

Sint32 main(void) {
 struct sigaction action;
 pthread_mutexattr_t attr;
 char buf[MAX_CHARS];

 sigemptyset(&action.sa_mask);
 action.sa_flags = 0;
 action.sa_handler = terminate;
 sigaction(SIGTERM, &action, NULL);
 sigaction(SIGINT, &action, NULL);
 sigaction(SIGQUIT, &action, NULL);



 globl_flags = 0;
 globl_fg.r = 
  globl_fg.g = 
  globl_fg.b = 0;
 globl_bg.r=
  globl_bg.g = 194;
 globl_bg.b = 184;
 globl_move_color.r = 90;
 globl_move_color.g = 32;
 globl_move_color.b = 24;
 globl_wait_tick = 1;

 set_defaults();
 snprintf(buf, MAX_CHARS, "%s/.hrr/hrr.conf", getenv("HOME"));
 if(load_config(buf) == 0)
  load_config("/etc/hrr.conf");

 zoom = 1.0f/config_file.start_scale;

 init_gui(config_file.screen_w, config_file.screen_h,config_file.fullscreen*FULLSCREEN);
 SDL_WM_SetCaption("HRR!!", "HRR!!");
 SDL_WM_SetIcon(IMG_ReadXPMFromArray(icon_xpm), NULL);

 printf("0x%x\n", gui_screen);

 setup_windows(config_file.screen_w, config_file.screen_h);
 setup_main_obj();
 if(config_file.do_splash==1)
  setup_splash();
 setup_time_bar();

 snprintf(tmp_path, MAX_CHARS, "/tmp/hrr_%d", getpid());
 clear_dir(tmp_path);
 if(mkdir(tmp_path, 0644)<0) {
  perror(tmp_path);
  exit(-1);
 }


// HOST_BLOCK;

 pthread_mutexattr_init(&attr);
 if(pthread_mutex_init(&mtx, &attr)<0) {
  perror("mutex");
  exit(-1);
 } 

 init_not(HOST, host_msg_recv);

 new_thread(loader);
// coder_thread = new_thread(coder_main);
// player_thread = new_thread(player_main);



// if(wocka==1)
//  new_thread(wocka_wocka);

 cursor.track = 1;
 cursor.pos = 60.0f;

// new_thread(update_memory_bar);
 group_loop(main_grp);
 terminate(SIGTERM, NULL, NULL);
}

void loader(void) {
 while(main_grp->go==0) SDL_Delay(40);
 printf("we are a go\n");
 coder_thread = new_thread(coder_main);
 player_thread = new_thread(player_main);
 if(wocka==1)
  new_thread(wocka_wocka);
 new_thread(update_memory_bar);
 return;
}

void update_memory_bar(void) {
 for(;;) {
  send_host(NOT_MEMORY_BAR, NULL, 0, STORE);
  SDL_Delay(2000);
 }
}

/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgw4ACgkQMNO4A6bnBrNW2wCgi3Rb9WdU6Y+9Rma4SYALren6
L5oAmQH9AF69CyM+S6q5qu1a+z2pkb0A
=MC0u
-----END PGP SIGNATURE-----
*/
