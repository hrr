/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#define BIRTHDAY	"Mon Sep 14 16:14:11 CDT 2009"

#define PROT \
 pthread_mutex_lock(&mtx); \
 HOST_BLOCK;

#define UNPROT \
 pthread_mutex_unlock(&mtx); \
 HOST_UNBLOCK;

#define TRACK_SIZE      175
#define TRACKS		6
#define STACK_SIZE 	5

#define MAX_CHARS	1024

#define VX(X)	(Sint32)(((float)X/800.0f)*(float)gui_screen->w)
#define VY(X)	(Sint32)(((float)X/600.0f)*(float)gui_screen->h)

#define fround(x) ((x)>=0.0f?(Sint32)((x)+0.5f):(Sint32)((x)-0.5f))

typedef struct {
 void *grp;
 void *gc;
 void *updater;
} stk_t;

extern stk_t stk[STACK_SIZE];
extern Sint32 stk_pointer;

extern struct select_file_t *cue_file;

#define POP_TIMEBAR \
 stk[stk_pointer].grp = current_grp; \
 stk[stk_pointer].gc = gc; \
 stk[stk_pointer++].updater = intern_update; \
 current_grp = time_bar_grp; \
 gc = time_bar_bmp; \
 intern_update = sub_update; 

#define POP_SUB \
 stk[stk_pointer].grp = current_grp; \
 stk[stk_pointer].gc = gc; \
 stk[stk_pointer++].updater = intern_update; \
 current_grp = sub_grp; \
 gc = sub; \
 intern_update = sub_update; 

#define POP_MAIN \
 stk[stk_pointer].grp = current_grp; \
 stk[stk_pointer].gc = gc; \
 stk[stk_pointer++].updater = intern_update; \
 current_grp = main_grp; \
 gc = gui_screen; \
 intern_update = norm_update; 

#define PUSH \
 current_grp = stk[--stk_pointer].grp; \
 gc = stk[stk_pointer].gc; \
 intern_update = stk[stk_pointer].updater; 

extern struct menu_entry_t menu_button_text[];

extern char tmp_path[MAX_CHARS];

extern struct object_t *vert, *time_bar, *main_obj; 

extern struct object_t *start_obj, *stop_obj, *pause_obj, *play_obj, *end_obj, *rec_obj;
extern struct object_t *progress_bar, *message_bar, *memory_bar;

extern SDL_Surface *ctx_old_gc, *time_bar_bmp;
extern group_t *ctx_old;
void *ctx_updater;

extern group_t *main_grp, *sub_grp, *splash_grp, *time_bar_grp, *menu_grp, *menu_wave_grp;

void terminate(int sigraised, siginfo_t *info, void *a);

void update_memory_bar(void);

void loader(void);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgw8ACgkQMNO4A6bnBrNrJgCeMdzy4k7D1UjEO007JZJcHxNW
RXcAnjvHYa2dekAnAfrDnm15aT7UbnJr
=YYlK
-----END PGP SIGNATURE-----
*/
