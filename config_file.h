/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#define		SIGNED_16BIT	1
#define		UNSIGNED_16BIT	2
#define		SIGNED_8BIT	3
#define 	UNSIGNED_8BIT	4	

typedef struct {
 char path[MAX_CHARS];
 Sint32 frequency;
 Sint32 channels;
 Sint32 format;
} audiodev_t;

extern struct config_t {
 audiodev_t in,out;
 Sint32	screen_w, screen_h;
 Sint32	do_splash;
 Sint32	fullscreen;
 float  start_scale;
} config_file;


extern Sint32 wocka;

Sint32 load_config(char *path);

void wocka_wocka(void);
void set_defaults();
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwwACgkQMNO4A6bnBrNLWACfdjTO2nLaxcErKv7HAk5S3MwK
ockAnRtLcpf1F9zOksdXormULYs0zEAQ
=+GWY
-----END PGP SIGNATURE-----
*/
