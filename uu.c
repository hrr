/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>

unsigned char buffer[0xffff];



Sint32 uudec(char *filename, char *name) {
}

static Sint32 
decode(char *name)
{
}

static Sint32 
decode2(char *name)
{
}

static Sint32 
uu_decode(void)
{
}



Sint32 in_read(char *out_buf, Sint32 len) {
}

Sint32 uuenc(FILE *fp, char *name, unsigned char *buf, Sint32 len)
{

}

void
encode(void)
{
}

/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgxQACgkQMNO4A6bnBrOLyACePiqxqpVKfkDPn6oD6I5iby8l
9UEAoIggDsx+odRZA3fNO1aFwzEsql3v
=UcVu
-----END PGP SIGNATURE-----
*/
