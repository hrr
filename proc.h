/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#define TRACK_ANY	-1

enum {
 CHUNKS,
 SECONDS,
 MINUTES,
 HOURS,
 DAYS
};

extern Sint32 current_scale;

#define TOO_LONG	"What are you kidding me? WAY TOO LONG!!"

#define MSG_CLEAN	MSG_USER

#define PROGRESS_BAR_UNDETER	0
#define PROGRESS_BAR_DETER	1

extern char message_bar_null;

typedef struct {
 struct object_t *wave;
 struct object_t *cue;
 Sint32 dirty, empty;
} track_t;

extern track_t tracks[TRACKS];

typedef struct {
 Sint32 track;
 double pos, end;
} cursor_t;

extern cursor_t cursor;

extern float zoom;
extern float globl_time;


extern SDL_Surface *sub, *left, *right;

void norm_update(struct object_t *obj);
void sub_update(struct object_t *obj);
Sint32 proc_main(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_wave(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_back(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_time_bar(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_start_splash(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_end_splash(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_progress_bar(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_message_bar(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_memory_bar(Sint32 msg, struct object_t *obj, Sint32 data);
void new_cue(char *name, Sint32 file_index, Sint32 track);

/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgxEACgkQMNO4A6bnBrMZSACfWoNlC4TGAOKPgB5I5OexVz8Y
200AoJTbvxeHtsV1HnMbl1k80hZQl7nS
=Gy+J
-----END PGP SIGNATURE-----
*/
