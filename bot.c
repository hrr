/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "includes.h"

#include "help_text.h"

#define CLICKY_OBJECT(X) \
 X->param.d1 = FALSE; \
 MESSAGE_OBJECT(X, MSG_DRAW); \
 intern_update(X); \
 fill_box(X->param.x, X->param.y,\
          X->param.x+X->param.w, X->param.y+X->param.h, \
	    &globl_fg, &globl_bg, XOR); \
   intern_update(X); \
   while(wait_on_mouse()!= MOUSE_UP); \
   fill_box(X->param.x, X->param.y, \
            X->param.x+X->param.w, X->param.y+X->param.h, \
	    &globl_fg, &globl_bg, XOR); \
   intern_update(X);

Sint32 current_tool;

Sint32 nah_really(struct object_t *obj, Sint32 data) {
 if(prompt(gui_screen->w/2, gui_screen->h/2, 
    "       For real?       ",
    "Quit :O", "Nope :)")==TRUE){
  return RET_QUIT;
 }
 return RET_OK;
}

void mover(Sint32 cmd) {
  send_coder(1337, NULL, 0, STORE);
}

Sint32 menu_bottom(struct object_t *obj, Sint32 data) {
 Sint32 i;
 double q;
 switch(obj->param.user_flags) {
  case MENU_WAVE_ZOOMTO:
   globl_time = cursor.pos;
   q = cursor.end - cursor.pos;
   if(q == 0.0f) q = 1.0f/ 512.0f; 
   zoom = 5.0f/ q;
   break;
 }
 return RET_QUIT;
}

Sint32 floating_track;

Sint32 bottom(struct object_t *obj, Sint32 data) {
 Sint32 i;
 switch(obj->param.user_flags) {
  case CMD_CUE:
   floating_track = obj->param.d2;
   POP_SUB;
   CLICKY_OBJECT(obj);
   PUSH;
   if(nb_input_files>= MAX_FILES) {
    alert(gui_screen->w/2, gui_screen->h/2,"Too many files cued", "Aww");
    break;
   }
   POP_MAIN;
   do_overlay_window(cue_file);
   PUSH;
   break;
  case CMD_SELECT:
   current_tool = TOOL_SELECT;
   break;
  case CMD_ENVELOPE:
   current_tool = TOOL_ENVELOPE;
   break;
  case CMD_MOVE:
   current_tool = TOOL_MOVE;
   break;
  case CMD_VSCROLL:
   for(i=0;i<TRACKS;i++)
    if(tracks[i].dirty == 1) {
     POP_SUB;
     MESSAGE_OBJECT( tracks[i].wave, MSG_DRAW);
     PUSH;
    }
   MESSAGE_OBJECT(main_obj, MSG_DRAW);
   clipped_update(0,0,0,0);
//   UPDATE_OBJECT(main_obj);  
   break;
  case CMD_MANUAL:
   scroll_text_window(gui_screen->w/2, gui_screen->h/2, 600, 200, help_text, sizeof(help_text));
   break;
  case CMD_START:
   CLICKY_OBJECT(start_obj);
   break;
  case CMD_STOP:
   CLICKY_OBJECT(stop_obj);
   break;
  case CMD_PLAY:
   CLICKY_OBJECT(play_obj);
   break;
  case CMD_PAUSE:
   CLICKY_OBJECT(pause_obj);
   break;
  case CMD_END:
   CLICKY_OBJECT(end_obj);
   break;
  case CMD_MENU:
   group_loop(menu_grp);
   break;
 }

 return RET_OK;
}


void host_msg_recv(Sint32 msg, void *data, Sint32 len) {
 switch(msg) {
  case NOT_ANNOY_WINDOW:
   alert(random()%(gui_screen->w-30), random()%(gui_screen->h-30), data, "OK");
   break;
  case NOT_ALERT_WINDOW:
   alert(gui_screen->w/2, gui_screen->h/2, data, "OK");
   break;
  case NOT_PROGRESS_BAR:
   progress_bar->param.d2 = PROGRESS_BAR_DETER;
   progress_bar->param.d1 = *(Sint32 *)data;
   POP_MAIN;
   MESSAGE_OBJECT(progress_bar, MSG_DRAW);
   PUSH;
   break;
  case NOT_PROGRESS_BAR_TICK:
   progress_bar->param.d2 = PROGRESS_BAR_UNDETER;
   POP_MAIN;
   MESSAGE_OBJECT(progress_bar, MSG_DRAW);
   PUSH;
   break;
  case NOT_MESSAGE_BAR:
   if((Sint32)data == 0)
    message_bar->param.dp2 = &message_bar_null;
   else 
    message_bar->param.dp2 = data;
   
   POP_MAIN;
   MESSAGE_OBJECT(message_bar, MSG_DRAW);
   PUSH;
   break;
  case NOT_MEMORY_BAR:
   POP_MAIN;
   MESSAGE_OBJECT(memory_bar, MSG_DRAW);
   PUSH;
   break;
 }
}

Sint32 cue_file_bot(struct select_file_t *selector, char *filename) {
 Sint32 i;
 i = input_file(filename);
 if(i<0)
  return NOPE_TRY_AGAIN;
 else {
  send_host(NOT_MESSAGE_BAR, "", 0, STORE); 
  return LOAD_OK_QUIT;
 }
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwoACgkQMNO4A6bnBrOKDACfVw3E51sTOXg3S4lo/vMmeG0N
3CIAoJHZEkqADKHE5Nni3uKMAzOn2ulL
=6daZ
-----END PGP SIGNATURE-----
*/
