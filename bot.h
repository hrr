/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
enum { CMD_CUE,
       CMD_LIVE,
       CMD_MENU,
       CMD_MANUAL,
       CMD_SELECT,
       CMD_ENVELOPE,
       CMD_MOVE,
       CMD_START,
       CMD_STOP,
       CMD_PAUSE,
       CMD_PLAY,
       CMD_REC,
       CMD_END,
       CMD_HSCROLL,
       CMD_VSCROLL,
       CMD_PAGELEFT,
       CMD_PAGERIGHT };

enum { MENU_LOAD,
       MENU_SAVE,
       MENU_CLEAR,
       MENU_CLEAR_TRACK, 
       MENU_WAVE_COPY,
       MENU_WAVE_DELETE,
       MENU_WAVE_CLEAR,
       MENU_WAVE_PASTE,
       MENU_WAVE_CUT,
       MENU_WAVE_ZOOMTO};

enum { TOOL_SELECT,
       TOOL_ENVELOPE,
       TOOL_MOVE };

extern Sint32 current_tool;


void mover(Sint32 cmd);
Sint32 cue_file_bot(struct select_file_t *selector, char *filename);
Sint32 nah_really(struct object_t *obj, Sint32 data);
Sint32 bottom(struct object_t *obj, Sint32 data);
Sint32 menu_bottom(struct object_t *obj, Sint32 data);
void host_msg_recv(Sint32 msg, void *data, Sint32 len);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwsACgkQMNO4A6bnBrMZFgCfaWByG/EIcOCO0vuorigZabYa
HY8An0VfQoQwdHN0vRrnKA+Po7p7jzru
=0XpB
-----END PGP SIGNATURE-----
*/
