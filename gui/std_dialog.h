/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
/* flags */
#define SHOW_FOCUS		1
#define QUIT_BUTTON		2
#define CALL_BUTTON		4
#define INTERNAL1		8	
#define INVALID			16
#define INTERNAL2		32
#define TOGGLE			64
#define DROP_SHADOW		128
#define DROP_SHADOW_READY	256
#define DROP_ACCUM		512
#define CLICKED_DOWN		1024
#define LOAD_XPM_FROM_ARRAY	2048
#define MAX_CHARS		4096
#define SINGLE_RADIO		8192
#define INACTIVE		16384
#define MODULAR			32768
#define HEX			65536
#define BLUE			131072
#define	SDL_SURFACE_TYPE	262144
#define FADE			(262144<<1)

#define MAX_PROMPT_LINES	30

#define	MOVE_GROUP	3	

extern void (*intern_update)(struct object_t *obj);

void simple_window(group_t *grp, Sint32 w, Sint32 h);
Sint32 prompt(Sint32 cent_x, Sint32 cent_y, char *message, char *yes_text, char *no_text);
void alert(Sint32 cent_x, Sint32 cent_y, char *message, char *ok_text);
void scroll_text_window(Sint32 cent_x, Sint32 cent_y, Sint32 w, Sint32 h, char *text, Sint32 len); 
Sint32 proc_bitmap(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_scroll_bar(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_icon_button(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_radio_button(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_move_button(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_text(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_ctext(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_box(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_shadow_box(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_button_box(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_hash_box(Sint32 msg, struct object_t *obj, Sint32 data);
Sint32 proc_knob(Sint32 msg, struct object_t *obj, Sint32 data);

/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwgACgkQMNO4A6bnBrOk2ACfcf3rlDvTzj2JlC5lZhGVuYgA
P4EAninpwACxMXlqoMpVSI+ijfLxEFHk
=uQkS
-----END PGP SIGNATURE-----
*/
