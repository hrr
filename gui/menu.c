/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <SDL.h>
#include "gui_types.h"
#include "link.h"
#include "drop.h"
#include "gui.h"
#include "draw.h"
#include "std_dialog.h"
#include "font.h"
#include "pixel.h"
#include "timer.h"
#include "load_save.h"
#include "menu.h"

Sint32 menu_text(Sint32 msg, struct object_t *obj, Sint32 data) {
 switch(msg) {
  case MSG_DRAW:
   if(obj->in_focus == 1) {
    fill_box(obj->param.x, obj->param.y, 
             obj->param.x+obj->param.w, obj->param.y + obj->param.h, 
	     obj->param.fg, obj->param.bg, NO_HASH);
    draw_text(obj->param.x, obj->param.y, obj->param.dp1, obj->param.bg, obj->param.bg, NO_HASH,0);
   } else {
    fill_box(obj->param.x, obj->param.y, 
             obj->param.x+obj->param.w, obj->param.y + obj->param.h, 
	     obj->param.bg, obj->param.fg, NO_HASH);
    draw_text(obj->param.x, obj->param.y, obj->param.dp1, obj->param.fg, obj->param.bg, NO_HASH,0);
   }
   break;
  case MSG_INFOCUS:
  case MSG_OUTFOCUS:
   menu_text(MSG_DRAW, obj, data);
   break;
  case MSG_PRESS:
   if(CHECK_FLAG(obj->param.flags, CALL_BUTTON) == TRUE)
    return obj->param.callback(obj, 0);
   break;
 }
 return RET_OK;
}

Sint32 quit_proc(Sint32 msg, struct object_t *obj, Sint32 data) {
 if(msg == MSG_INFOCUS)
  return RET_QUIT;
 else
  return RET_OK;
}

group_t *new_menu(Sint32 x, Sint32 y, struct menu_entry_t *root, color_t *fg, color_t *bg) {
 group_t *new;
 struct object_t *tmp_object;
 obj_param_t tmp_parm;
 Sint32 w, h;
 Sint32 i,j;
 w = 10;
 for(i=0;;i++) {
  if(root[i].name!=NULL) {
   j = strlen(root[i].name);
   if(((j * 8)+10)>w)
    w = (j*8)+10;
  } else {
   break; 
  }
 }
 h = (i*8)+10;
 new = new_group(x, y, w+2, h+2, globl_flags, globl_drop_depth);
 PARM(0,0,w,h, fg, bg, 0, proc_shadow_box);
 new_obj(new, &tmp_parm); 

 PARM(0-x,0-y, x, gc->h, 0,0,0, quit_proc);
 new_obj(new, &tmp_parm);

 PARM(0, 0-y, w, y, 0,0,0, quit_proc);
 new_obj(new, &tmp_parm);

 PARM(0, h, w, gc->h -  h, 0,0,0, quit_proc);
 new_obj(new, &tmp_parm);

 PARM(w, 0-y, gc->w - w, gc->h, 0,0,0, quit_proc);
 new_obj(new, &tmp_parm);

 for(j=0;j<i;j++) {
  PARM(5,5+(j*8), w-10, 8, fg, bg, root[j].flags,menu_text);
  tmp_parm.dp1 = root[j].name; 
  tmp_parm.callback = root[j].callback;
  tmp_parm.user_flags = root[j].user_flags;
  new_obj(new, &tmp_parm);
 }

 return new;
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwUACgkQMNO4A6bnBrMTLQCdHzUyjTPID1X8kStWasK8ZarS
89MAn19Et+RYdLtgzROPNTts8S8oKZ/J
=mP83
-----END PGP SIGNATURE-----
*/
