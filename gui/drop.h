/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
/* at 35 we start to overflow into negative numbers */
#define MAX_DEPTH	80	

typedef struct {
 struct list_head node;
 Sint32 w,h;
 Sint32 depth;
 Uint8 *buffer;
} drop_t;

extern drop_t *drops;

void build_coef(void);
void draw_drop(SDL_Surface *dst, Sint32 x, Sint32 y, drop_t *drop, Sint32 w, Sint32 h, SDL_Rect *clip);
drop_t *new_drop(Sint32 depth);
void drop_init(void);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvwACgkQMNO4A6bnBrPhrACbBjVblSmJCbQz5E8vUQow/1Pf
eYUAn3v/Vfoy/mMarmESSjk1G2rCDdkf
=m4l9
-----END PGP SIGNATURE-----
*/
