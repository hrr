/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#define PARM(X,Y,W,H,FG,BG,FLAGS,PROC)	tmp_parm.x = X;\
				        tmp_parm.y = Y; \
					tmp_parm.w = W; \
					tmp_parm.h = H; \
					tmp_parm.fg = FG; \
					tmp_parm.bg = BG; \
					tmp_parm.flags = FLAGS; \
					tmp_parm.proc = PROC;

#define MAX_GAUSS_DEPTH	70

#define	MOUSE_MOVE	0
#define MOUSE_UP	1
#define MOUSE_DOWN	2
#define KEY_DOWN	3

#define MSG_DRAW	0
#define MSG_START	1
#define MSG_INFOCUS	2
#define MSG_OUTFOCUS	3
#define MSG_CLICK	4  // button down
#define MSG_UNCLICK	5  // button up
#define MSG_PRESS	6  // button actually pressed
#define MSG_RADIO	7 
#define MSG_CLEAR_INTERNAL1	8
#define MSG_RADIO2	9
#define MSG_CLEAR_INTERNAL2	10
#define MSG_KEYDOWN	11
#define MSG_KEYUP	12
#define MSG_RELOAD	13
#define MSG_TICK	14
#define MSG_DESTROY	15
#define MSG_MOUSEMOVE	16
#define MSG_USER	17
#define MSG_USER2	18
#define MSG_AFTERDRAW	19
#define MSG_NOP		20
#define MSG_RIGHT	21

#define FULLSCREEN	1

#define RET_QUIT	0
#define RET_OK		1

#define MESSAGE_OBJECT(q,x) q->param.proc(x, q, NULL)


extern pthread_mutex_t mtx;

extern Sint32 globl_flags;
extern Sint32 lock_update;
extern Sint32 globl_dirt, globl_quit_value;
extern Sint32 globl_drop_depth;

extern Sint32 floating_key;
extern SDL_Surface *gui_screen;
extern Sint32 gui_mouse_x, gui_mouse_y;
extern void (*globl_tick)(void);
extern void (*globl_wait_tick)(void);

typedef struct {
 unsigned char r,g,b;
} color_t;

extern color_t globl_fg, globl_bg;
extern color_t globl_move_color;

typedef struct {
 Sint32 (*proc)(Sint32 msg, struct object_t *obj, Sint32 data);
 Sint32 x,y;
 Sint32 w,h;
 color_t *fg, *bg;
 Sint32 d1, d2;
 Sint32 flags;
 void *dp1, *dp2, *dp3;
 Sint32 (*callback)(struct object_t *obj, Sint32 data);
 Sint32 quit_value;
 Sint32 pad2[128];
 Sint32 user_flags;
 Sint32 pad[128];
} obj_param_t;

struct object_t {
 struct list_head node;
 obj_param_t param;
 Sint32 in_focus, clicked, flags;
};

extern struct object_t *focus_obj;

typedef struct {
 Sint32 flags;
 Sint32 pos_x, pos_y;
 Sint32 w,h;
 Sint32 ready;
 struct object_t *objs;
 drop_t *drop_buffer;
// Uint8 *drop_buffer;
 Sint32 drop_w, drop_h, drop_d;
 int go;
} group_t;

extern group_t *current_grp;


void default_tick(void);
void null_tick(void);
Sint32 wait_on_mouse(void);
struct object_t *new_obj(group_t *grp, obj_param_t *param);
group_t *new_group(Sint32 x,Sint32 y, Sint32 w, Sint32 h, Sint32 flags, Sint32 drop_d);
void destroy_group(group_t *grp);
void group_loop(group_t *grp);
void broadcast_group(group_t *grp, Sint32 msg, Sint32 data);
void init_gui(Sint32 x, Sint32 y, Sint32 flags);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgv8ACgkQMNO4A6bnBrPq1gCfben4kBE27OrpTqHCHEGTD0A9
XfUAoJmNckNJzBleCOnyBqVzSat9uE0S
=S6vW
-----END PGP SIGNATURE-----
*/
