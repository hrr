/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#define NO_HASH	1	
#define HASH	2
/* ok so the above two are constants for non-text draw stuff */
#define XOR	3	

#define LOCK	if(SDL_MUSTLOCK(gui_screen) != 0) SDL_LockSurface(gui_screen)
#define UNLOCK	if(SDL_MUSTLOCK(gui_screen) != 0) SDL_LockSurface(gui_screen)

#define UPDATE_OBJECT(Q) clipped_update(current_grp->pos_x+Q->param.x, \
                         current_grp->pos_y + Q->param.y, \
                         current_grp->pos_x + Q->param.x + Q->param.w, \
                         current_grp->pos_y + Q->param.y + Q->param.h)

extern SDL_Surface *gc;

void clipped_udate(Sint32 x, Sint32 y, Sint32 w, Sint32 h);
void vline(Sint32 rx, Sint32 ry, Sint32 ry2, color_t *fg, color_t *bg, Sint32 type);
void hline(Sint32 rx, Sint32 ry, Sint32 rx2, color_t *fg, color_t *bg, Sint32 type);
void other_hline(Sint32 rx, Sint32 ry, Sint32 rx2, color_t *fg, color_t *bg, Sint32 type);
void fill_box(Sint32 x, Sint32 y, Sint32 x2, Sint32 y2, color_t *fg, color_t *bg, Sint32 type);
void box(Sint32 x, Sint32 y, Sint32 x2, Sint32 y2, color_t *fg, color_t *bg, Sint32 type);
void triangle(Sint32 xi1, Sint32 yi1, 
              Sint32 xi2, Sint32 yi2, 
              Sint32 xi3, Sint32 yi3, color_t *fg, color_t *bg, Sint32 type);

void clear_screen(void);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvoACgkQMNO4A6bnBrOJOQCeOR6lh7BPnUoFXHAHf2sdnWqy
VO4An1D11a0YBTOWW2v8fpL0Rvol/EiP
=YSF3
-----END PGP SIGNATURE-----
*/
