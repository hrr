/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include <math.h>
#include <pthread.h>
#include <SDL.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <dirent.h>
#include <signal.h>
#include "gui_types.h"
#include "link.h"
#include "drop.h"
#include "gui.h"
#include "draw.h"
#include "std_dialog.h"
#include "pixel.h"

#include "menu.h"


#include "../not/libnot.h"
#include "../main.h"

SDL_Surface *gc;

Sint32 updating=0;

void clipped_update(Sint32 x, Sint32 y, Sint32 w, Sint32 h) {
 Sint32 pw,ph;

 PROT;
 LOCK;
 if((x+y+w+h) == 0) 
  SDL_UpdateRect(gc, 0,0,0,0);
 else {

  pw = x+w;
  ph = y+h;

  if(x < 0 && pw > gc->w) {
   pw = gc->w;
   x = 0;
   SDL_UpdateRect(gc, 0,0,0,0);
   UNPROT;
   UNLOCK;
   return;
  } else {
   if(pw > gc->w) {
    if( x > gc->w) return;
    else
     pw = gc->w - x;
   } else
    pw = w;
   if(x < 0) x = 0;
  }
  if(y<0 && ph > gc->h) {
   ph = gc->h;
   y = 0;
   SDL_UpdateRect(gc, 0,0,0,0);
   UNLOCK;
   UNPROT;
   return;
  } else {
   if(ph > gc->h) {
    if( y > gc->h) {
     UNLOCK;
     UNPROT;
     return;
    } else
     ph = gc->h - y;
   } else
    ph = h;
   if(y < 0) y = 0;
   SDL_UpdateRect(gc,x,y, pw,ph);
  }
 }
 UNLOCK;
 UNPROT;
}

void vline(Sint32 rx, Sint32 ry, Sint32 ry2, color_t *fg, color_t *bg, Sint32 type) {
 Sint32 mask, *intp1, *intp2;
 Sint32 x,y,y2;
 Sint32 fg_color, bg_color;
 unsigned char *pix;
 Sint32 j,i;
 Sint32 hash_start;
// if(pthread_self() != host_thread) printf("Incorrect thread drawing stuff!\n");
 if( (rx+ current_grp->pos_x) < 0) 
  return; 
 if( (rx + current_grp->pos_x) >= gc->w)
  return;
 else
  x = rx;
 if( (ry+ current_grp->pos_y) < 0) 
  y = current_grp->pos_y * -1;
 else
  y = ry;
 if((ry2+ current_grp->pos_y) > gc->h) 
  y2 = (gc->h - current_grp->pos_y)-1;
 else
  y2 = ry2;

 pix = (unsigned char *)((Uint8 *)gc->pixels + 
                         (gc->pitch * (y+current_grp->pos_y)) +
			 (gc->format->BytesPerPixel * (x+current_grp->pos_x)) 
                        );

 mask = 0;
 for(j=0;j<gc->format->BytesPerPixel;j++)
  mask |= 0xff << (8*j);

#ifdef WORDS_BIGENDIAN
 mask<<=8;
#endif

 pthread_mutex_lock(&mtx);
 LOCK;
 switch(type) {
  case XOR:
   for(i = y;i<y2;i++)  {
    intp1 = (void *)pix;
    *intp1^=mask;
    pix+=gc->pitch;
   }
   break;
  case HASH:
   hash_start = (x^y)&1;
   mask^=~0;
   fg_color = SDL_MapRGB(gc->format, fg->r, fg->g, fg->b);
   bg_color = SDL_MapRGB(gc->format, bg->r, bg->g, bg->b);
#ifdef WORDS_BIGENDIAN
   fg_color <<=8;
   bg_color <<=8;
#endif
   for(i = y;i<y2;i++) {
    if(hash_start == 1)
     intp2 = &fg_color;
    else
     intp2 = &bg_color;
    hash_start^=1;
    intp1 = (void *)pix; 
    *intp1&=mask;
    *intp1+=*intp2;
    pix+=gc->pitch;  
   }
   break;
  case NO_HASH:
   mask^=~0;
   fg_color = SDL_MapRGB(gc->format, fg->r, fg->g, fg->b);
#ifdef WORDS_BIGENDIAN
   fg_color<<=8;
#endif
   for(i=y;i<y2;i++){
    intp1 = (void *)pix;
    *intp1&=mask;
    *intp1+=fg_color;
    pix+=gc->pitch;
   }
   break;
 }
 UNLOCK;
 pthread_mutex_unlock(&mtx);
 globl_dirt = 1;
}
 
void hline(Sint32 rx, Sint32 ry, Sint32 rx2, color_t *fg, color_t *bg, Sint32 type) {
 Sint32 mask, *intp1, *intp2;
 Sint32 x,y,x2;
 Sint32 fg_color, bg_color;
 unsigned char *pix;
 Sint32 j,i;
 Sint32 hash_start;

 if( (rx+ current_grp->pos_x) < 0 ) 
  x = current_grp->pos_x * -1;
 else
  x = rx;

 if( (ry+ current_grp->pos_y) < 0 ) 
  return;
 
 if( (ry+current_grp->pos_y) >= gc->h) 
   return;
 else
  y = ry;

 if((rx2+current_grp->pos_x) > gc->w) 
  x2 = (gc->w - current_grp->pos_x)-1;
 else 
  x2 = rx2;

 pix = (unsigned char *)((Uint8 *)gc->pixels + 
                         (gc->pitch * (y+current_grp->pos_y)) +
			 (gc->format->BytesPerPixel * (x+current_grp->pos_x)) 
                        );

 mask = 0;
 for(j=0;j<gc->format->BytesPerPixel;j++)
  mask |= 0xff << (8*j);
#ifdef WORDS_BIGENDIAN
 mask<<=8;
#endif
 LOCK;
 pthread_mutex_lock(&mtx);
 switch(type) {
  case XOR:
   if((y == (gc->h-1)) && (x2 > (gc->w-1))) 
    x2--; 
   for(i = x;i<x2;i++)  {
    intp1 = (void *)pix;
    *intp1^=mask;
    pix+=gc->format->BytesPerPixel;
   }
   break;
  case HASH:
   hash_start = (x^y)&1;
   mask^=~0;
   fg_color = SDL_MapRGB(gc->format, fg->r, fg->g, fg->b);
   bg_color = SDL_MapRGB(gc->format, bg->r, bg->g, bg->b);
#ifdef WORDS_BIGENDIAN
   fg_color<<=8;
   bg_color<<=8;
#endif
   if((y == (gc->h-1)) && (x2 > (gc->w-1))) 
    x2--;
   for(i = x;i<x2;i++) {
    if(hash_start == 1)
     intp2 = &fg_color;
    else
     intp2 = &bg_color;
    hash_start^=1;
    intp1 = (void *)pix; 
    *intp1&=mask;
    *intp1+=*intp2;
    pix+=gc->format->BytesPerPixel;  
   }
   break;
  case NO_HASH:
   mask^=~0;
   fg_color = SDL_MapRGB(gc->format, fg->r, fg->g, fg->b);
#ifdef WORDS_BIGENDIAN
   fg_color<<=8;
#endif
   if((y == (gc->h-1)) && (x2 > (gc->w-1))) 
    x2--; 
   for(i=x;i<x2;i++){
    intp1 = (void *)pix;
    *intp1&=mask;
    *intp1+=fg_color;
    pix+=gc->format->BytesPerPixel;
   }
   break;
 }
 pthread_mutex_unlock(&mtx);
 UNLOCK;
 globl_dirt = 1;

}

void fill_box(Sint32 x, Sint32 y, Sint32 x2, Sint32 y2, color_t *fg, color_t *bg, Sint32 type) {
 Sint32 i, mask, *intp1, *intp2;
 Sint32 fg_color, bg_color, hash_start;
 Uint8 *pix;
 Sint32 rx,ry,rx2,ry2;
 Sint32 s,t,j;

 if( (x+ current_grp->pos_x) >= gc->w)
  return;

 if( (y+ current_grp->pos_y) >= gc->h)
  return;

 if( (x+ current_grp->pos_x) < 0)  
  rx = current_grp->pos_x * -1;
 else
  rx = x;

 if( (y+ current_grp->pos_y) < 0) 
  ry = current_grp->pos_y * -1;
 else
  ry = y;

 if((x2+current_grp->pos_x) > gc->w) 
  rx2 = (gc->w - current_grp->pos_x) -1;
 else 
  rx2 = x2;

 if((y2+ current_grp->pos_y) > gc->h) 
  ry2 = (gc->h - current_grp->pos_y)-1;
 else
  ry2 = y2;

 pix = (Uint8 *)((Uint8 *)gc->pixels + 
                         (gc->pitch * (ry+current_grp->pos_y)) +
			 (gc->format->BytesPerPixel * (rx+current_grp->pos_x)) 
                        );
 mask = 0;
 for(j=0;j<gc->format->BytesPerPixel;j++)
  mask |= 0xff << (8*j);
#ifdef WORDS_BIGENDIAN
 mask<<=8;
#endif

 LOCK;
 pthread_mutex_lock(&mtx);
 switch(type) {
  case XOR:

   if((ry2 > (gc->h-1)) && (rx2 > (gc->w-1))) {
    rx2--;
    ry2--;
   }

   for(t=ry;t<ry2;t++) {
    for(s=rx;s<rx2;s++) {
     intp1 = (void *)pix;
     *intp1^=mask;
     pix+=gc->format->BytesPerPixel;  
    }
    pix-=gc->format->BytesPerPixel * (rx2-rx);
    pix+=gc->pitch;
   }
   break;
  case HASH:
   mask^=~0;
   fg_color = SDL_MapRGB(gc->format, fg->r, fg->g, fg->b);
   bg_color = SDL_MapRGB(gc->format, bg->r, bg->g, bg->b);
#ifdef WORDS_BIGENDIAN
   fg_color<<=8;
   bg_color<<=8;
#endif
   hash_start = (rx^ry)&1;

   if((ry2 > (gc->h-1)) && (rx2 > (gc->w-1))) {
    rx2--;
    ry2--;
   }

   for(t=ry;t<ry2;t++) {
    for(s=rx;s<rx2;s++) {
     if(hash_start == 1) 
      intp2 = &fg_color;
     else
      intp2 = &bg_color;
     hash_start^=1;
     intp1 = (void *)pix;
     *intp1&=mask;
     *intp1+=*intp2;
     pix+=gc->format->BytesPerPixel;  
    }
    pix-=gc->format->BytesPerPixel * (rx2-rx);
    pix+=gc->pitch;
    hash_start = (s^t)&1;
   }

   break;
  case NO_HASH:
   mask^=~0;
   fg_color = SDL_MapRGB(gc->format, fg->r, fg->g, fg->b);
#ifdef WORDS_BIGENDIAN
   fg_color<<=8;
#endif
   intp2 = &fg_color;

   if((ry2 > (gc->h-1)) && (rx2 > (gc->w-1))) {
    rx2--;
    ry2--;
   }

   for(t=ry;t<ry2;t++) {
    for(s=rx;s<rx2;s++) {
     intp1 = (void *)pix;
     *intp1&=mask;
     *intp1+=*intp2;
     pix+=gc->format->BytesPerPixel;  
    }
    pix-=gc->format->BytesPerPixel * (rx2-rx);
    pix+=gc->pitch;
   }

   break;
 }
 UNLOCK;
 pthread_mutex_unlock(&mtx);
 globl_dirt = 1; 
}

void box(Sint32 x, Sint32 y, Sint32 x2, Sint32 y2, color_t *fg, color_t *bg, Sint32 type) {
 vline(x,y, y2, fg, bg,   type);
 vline(x2,y,y2, fg, bg,   type);
 hline(x,y, x2, fg, bg,   type);
 hline(x,y2,x2+1, fg, bg, type);
}

/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvoACgkQMNO4A6bnBrP+0gCfRPccLTruEhvGX6cND3snczOY
Gc0AoIXH3IiIHlWo2YPd1AtzotHiqy3g
=kt8A
-----END PGP SIGNATURE-----
*/
