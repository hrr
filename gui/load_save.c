/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <SDL.h>
#include "gui_types.h"
#include "link.h"
#include "drop.h"
#include "gui.h"
#include "draw.h"
#include "std_dialog.h"
#include "font.h"
#include "pixel.h"
#include "timer.h"
#include "load_save.h"

#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "config.h"

/*                  ______ 
 *               .-"      "-. 
 *              /            \ 
 *  _          |              |          _ 
 * ( \         |,  .-.  .-.  ,|         / )
 *  > "=._     | )(__/  \__)( |     _.=" <
 * (_/"=._"=._ |/     /\     \| _.="_.="\_)
 *        "=._ (_     ^^     _)"_.=" 
 *            "=\__|IIIIII|__/="
 *           _.="| \IIIIII/ |"=._
 * _     _.="_.="\          /"=._"=._     _
 *( \_.="_.="     `--------`     "=._"=._/ )
 * > _.="                            "=._ <
 *(_/                                    \_)
 *              NOW HEAR THIS
 *         This code is hacked to  
 *        bits. Clean before hacking
 */


Sint32 do_load(struct select_file_t *selector, char *filename);
Sint32 do_save(struct select_file_t *selector, char *filename);

#define BREAKER 0
#define QUITER	4

Sint32 fix_file(struct select_file_t *parent, char *filename) {
 char *cp;
 Sint32 i;
 char new_path[BIGBUF];
 int fp;
 struct stat qstat;

 if(parent->path[1] != 0) { 
  if( parent->path[strlen(parent->path) - 1] != '/') 
   snprintf(new_path, BIGBUF, "%s/%s", parent->path, filename);
  else
   snprintf(new_path, BIGBUF, "%s%s", parent->path, filename);
 } else { 
  if(strncmp("..", filename, 3) != 0)
   snprintf(new_path, BIGBUF, "%s%s", parent->path, filename);
  else {
   parent->selected_line = -1;
   cp = (char *)parent->name_object->param.dp1;
   *cp = 0;
   MESSAGE_OBJECT(parent->name_object, MSG_START);
   MESSAGE_OBJECT(parent->name_object, MSG_DRAW);
   return BREAKER;
  }
 }

 fp = open(new_path, O_RDONLY);

 if(parent->type == LOAD){
  if( fp < 0 )
   return BREAKER;
 } else {
  if( fp < 0) 
   if(do_save(parent, new_path) == RET_QUIT)
    return QUITER;
   else
    return BREAKER;
 }

 fstat(fp, &qstat);
 close(fp);
 if(S_ISREG(qstat.st_mode) != 0) {
  if(parent->type == LOAD) {
   if( do_load(parent, new_path) == RET_QUIT)
    return QUITER;
  } else {
   if( prompt(gui_screen->w/2, gui_screen->h/2,
              "So you want to overwrite this file?",
                        "Yes", "No") == TRUE) {
    if( do_save(parent, new_path) == RET_QUIT)
     return QUITER;
   }
  }
 }
 if(S_ISDIR(qstat.st_mode) != 0) {
  if(strncmp(".", filename,2) == 0) {
   parent->selected_line = -1;
   cp = (char *)parent->name_object->param.dp1;
   *cp = 0;
   MESSAGE_OBJECT(parent->name_object, MSG_START);
   MESSAGE_OBJECT(parent->name_object, MSG_DRAW);
   return BREAKER;
  } 
  if(strncmp("..", filename,3) == 0) {
   for(i = strlen(new_path) - 4;;i--)
    if(new_path[i] == '/') {
     if(i != 0)
       new_path[i] = 0;
     else
       new_path[i+1] = 0;
     break;
   }
  }
  
  for(i=0;i<BIGBUF;i++) {
   parent->path[i] = new_path[i];
   if(parent->path[i] == 0) break;  }
  read_dir(parent);
  broadcast_group(parent->grp, MSG_DRAW, 0);
 }
 return BREAKER; 
}

Sint32 line_edit(Sint32 msg, struct object_t *obj, Sint32 data) {
 char *cp;
 int fp;
 Sint32 i;
 struct stat qstat;
 char name[BIGBUF];
 struct select_file_t *parent;
 color_t tmp_color;
 parent = (struct select_file_t *)obj->param.dp2;
 switch(msg) {
  case MSG_START:
   obj->param.d1 = 0;
   obj->param.d2 = strlen((char *)obj->param.dp1);
   break;
  case MSG_TICK:
   obj->param.d1^=1;
  case MSG_DRAW:
  if(obj->in_focus == 0) {
   fill_box(obj->param.x, obj->param.y,
     obj->param.x + obj->param.w, obj->param.y+obj->param.h, obj->param.bg, obj->param.bg, NO_HASH);
   tmp_color.r = obj->param.fg->r;
   tmp_color.g = obj->param.fg->g;
   tmp_color.b = obj->param.fg->b;
  } else {
   tmp_color.r = obj->param.bg->r/4;
   tmp_color.g = obj->param.bg->g/4;
   tmp_color.b = obj->param.bg->b;
   fill_box(obj->param.x, obj->param.y,
     obj->param.x+obj->param.w, obj->param.y+obj->param.h, &tmp_color, &tmp_color, NO_HASH);
   tmp_color.r = obj->param.bg->r;
   tmp_color.g = obj->param.bg->g;
  }
  box(obj->param.x, obj->param.y,
    obj->param.x + obj->param.w, obj->param.y+obj->param.h, obj->param.fg, obj->param.bg, NO_HASH);
  if(CHECK_FLAG(obj->param.flags, MAX_CHARS) == TRUE)  
   draw_text(obj->param.x+2, obj->param.y+(obj->param.h/2)-4,
     (char *)obj->param.dp1, &tmp_color, &tmp_color,
     NO_HASH|MAX_CHARS,(obj->param.w/8)-1);
  else
   draw_text(obj->param.x+2, obj->param.y+(obj->param.h/2)-4,
     (char *)obj->param.dp1, &tmp_color, &tmp_color, NO_HASH,0);
   if(obj->in_focus == 1 && obj->param.d1 == 1) 
    fill_box(obj->param.x+2 +(8*obj->param.d2), obj->param.y+(obj->param.h/2)-4,
             obj->param.x+10+(8*obj->param.d2), obj->param.y+(obj->param.h/2)+5,
	     &globl_bg, &globl_bg, NO_HASH);
  break;
  case MSG_INFOCUS:
  case MSG_OUTFOCUS:
   if(CHECK_FLAG(obj->param.flags, SHOW_FOCUS) == TRUE)
    line_edit(MSG_DRAW, obj, data);
  break;
  case MSG_KEYDOWN:
  /* XXX scroll the text area for more text please */
   cp = obj->param.dp1;
   if( data == SDLK_RETURN) {
    if(parent->name[0] == 0) break;
    if(parent->name[0] == '/') {
     fp = open(parent->name, O_RDONLY);
     if(parent->type == LOAD) {
      if(fp<0) {
       cp = (char *)parent->name_object->param.dp1;
       *cp = 0;
       MESSAGE_OBJECT(parent->name_object, MSG_START);
       MESSAGE_OBJECT(parent->name_object, MSG_DRAW);
       break;
      }
     } else {
      if(fp<0) {
       if( do_save(parent, parent->name) == RET_QUIT)
	return RET_QUIT;
       break;
      }
     }
     fstat(fp, &qstat);
     close(fp);
     if(S_ISREG(qstat.st_mode) != 0) {
      if(parent->type == LOAD) {
       if( do_load(parent, parent->name) == RET_QUIT)
        return RET_QUIT;
      } else {

       if( prompt(gui_screen->w/2, gui_screen->h/2,
               "So you want to overwrite this file?",
                        "Yes", "No") == TRUE) {
        if( do_save(parent, parent->name) == RET_QUIT)
         return RET_QUIT;
       } else
	return RET_QUIT;
 

      }
     } 
     if(S_ISDIR(qstat.st_mode) != 0){
       for(i=0;i<BIGBUF;i++) {
        parent->path[i] = parent->name[i];
        if(parent->path[i] == 0) break;
       } 
       read_dir(parent);
       cp = (char *)parent->name_object->param.dp1;
       *cp = 0;
       MESSAGE_OBJECT(parent->name_object, MSG_START);
       broadcast_group(parent->grp, MSG_DRAW,0);
       break;
     }
    }
    i = strlen(parent->name);
    if( parent->name[i-1] == '/') parent->name[i-1] = 0; 
    if(fix_file(parent, parent->name) == QUITER)
     return RET_QUIT;
    break;


   } else { 
    if( data == SDLK_BACKSPACE) {
     obj->param.d2--;
     if(obj->param.d2 < 0)
      obj->param.d2 = 0;
     else
      cp[obj->param.d2] = 0;
      
    } else {
     cp[obj->param.d2++] = data;
     cp[obj->param.d2] = 0;
    }
   }
   MESSAGE_OBJECT(obj, MSG_DRAW);
   break;
 
 }
 return RET_OK;
}

Sint32 text_hilight(Sint32 msg, struct object_t *obj, Sint32 data) {
 Sint32 q;
 Sint32 i;
 int fp;
 Sint32 w;
 char *cp;
 struct stat qstat;
 struct select_file_t *parent;

 color_t blue;
 color_t *bg_color;

// char new_path[BIGBUF];

 if(CHECK_FLAG(obj->param.d2, NO_TEXT) == TRUE) return RET_OK;

 w = (obj->param.w / 8) -1;

 parent = obj->param.dp1;
 switch(msg) {
  case MSG_DRAW:

   if((parent->selected_line - parent->scroll_bar->param.d1 ) == obj->param.d1) {

    fill_box(obj->param.x, obj->param.y,
              obj->param.x+ obj->param.w, obj->param.y + obj->param.h, 
              obj->param.fg, obj->param.bg , NO_HASH);

    draw_text(obj->param.x, obj->param.y, obj->param.dp2, obj->param.bg, obj->param.bg, 
      NO_HASH|MAX_CHARS, w);
 
   } else {
    if(CHECK_FLAG(obj->param.d2,CLICKED)==TRUE) {
     fill_box(obj->param.x, obj->param.y,
              obj->param.x+ obj->param.w, obj->param.y + obj->param.h, 
              &globl_move_color, obj->param.bg , NO_HASH);
     draw_text(obj->param.x, obj->param.y, obj->param.dp2, obj->param.bg, obj->param.bg, 
       NO_HASH|MAX_CHARS, w);
    } else {
     blue.r =0x7f;
     blue.g =0x9f;
     blue.b =0xff;
     if(((obj->param.d1+parent->scroll_bar->param.d1)&2)==2)
      bg_color = &blue;
     else
      bg_color = obj->param.bg;
     fill_box(obj->param.x, obj->param.y,
              obj->param.x+ obj->param.w, obj->param.y + obj->param.h, 
	      bg_color, obj->param.bg , NO_HASH);

     if(obj->in_focus == TRUE) 
      fill_box(obj->param.x, obj->param.y,
               obj->param.x+ obj->param.w, obj->param.y + obj->param.h, 
   	       &globl_move_color, obj->param.bg, HASH);
     draw_text(obj->param.x, obj->param.y, obj->param.dp2, obj->param.fg, obj->param.bg, 
       NO_HASH|MAX_CHARS, w);
    }
   }
   break;
  case MSG_CLICK:
   obj->param.d2 |= CLICKED;
   text_hilight(MSG_DRAW,obj,NULL);
   break;
  case MSG_OUTFOCUS:
  case MSG_UNCLICK:
   obj->param.d2 &= CLICKED^~0;
   text_hilight(MSG_DRAW,obj,NULL);
   break;
  case MSG_INFOCUS:
   text_hilight(MSG_DRAW,obj,NULL);
   break;
  case MSG_PRESS:
   if(parent->selected_line == obj->param.d1 + parent->scroll_bar->param.d1) {
    if(fix_file( parent, obj->param.dp2) == QUITER)
     return RET_QUIT;
    break;
   }
   if(parent->selected_line != -1) {
    q = parent->selected_line - parent->scroll_bar->param.d1;
     if(q < parent->nlines && q>=0) {
     parent->selected_line = -1;
     cp = (char *)parent->name_object->param.dp1;
     *cp = 0;
     MESSAGE_OBJECT(parent->name_object,MSG_START);
     MESSAGE_OBJECT(parent->lines[q], MSG_DRAW);
     UPDATE_OBJECT(parent->lines[q]);
    }
   } 
   parent->selected_line = obj->param.d1 + parent->scroll_bar->param.d1;
   cp = (char *)obj->param.dp2;
   for(i=0;;i++) {
    parent->name[i] = cp[i];
    if(parent->name[i] == 0) break;
   }
   fill_box(2,35,parent->grp->w-2,9,&globl_bg, &globl_bg,NO_HASH);
   MESSAGE_OBJECT( parent->name_object, MSG_START);
   MESSAGE_OBJECT( parent->name_object, MSG_DRAW);
   text_hilight(MSG_DRAW,obj,NULL);
   UPDATE_OBJECT(obj);
   break;
 }
 return RET_OK;
}

Sint32 selector_bot(struct object_t *obj, Sint32 data) {
 Sint32 i;
 struct select_file_t *parent;
 parent = (struct select_file_t *)obj->param.dp1;
 for(i = 0; i<parent->nlines;i++) {
  parent->lines[i]->param.dp2 = (void *)parent->text_lines[i+ obj->param.d1];
  MESSAGE_OBJECT(parent->lines[i], MSG_DRAW);
 }
 clipped_update(parent->grp->pos_x, parent->grp->pos_y, 
                parent->grp->pos_x+ parent->grp->w, parent->grp->pos_y+parent->grp->h);
 return RET_OK;
}


int qsort_cmp(void *A, void *B) {
 return strncmp(A,B,MEDBUF);
}

void read_dir(struct select_file_t *selector) {
 Sint32 i,len;

 char *buf, *ebuf, *cp;
 long base;
 size_t bufsize;
 Sint32 fd, nbytes;
 char *path;
 struct stat sb;
 struct dirent *dp;


 len = 0;
 if(( fd = open(selector->path, O_RDONLY)) < 0) return;
 fstat(fd, &sb);
 bufsize = sb.st_size;
 if(bufsize < sb.st_blksize);
  bufsize = sb.st_blksize;
  buf = malloc(bufsize);
  while((nbytes = getdirentries(fd,buf,bufsize,&base))>0) {
   ebuf = buf+nbytes;
   cp = buf;
   while(cp<ebuf) {
    dp = (struct dirent *)cp;
    if(dp->d_fileno!=0) {
     for(i=0;;i++) {
      selector->text_lines[len][i] = dp->d_name[i];
      if(dp->d_name[i] == 0) break;
     }  
     len++; 
     if(len == BIGBUF) {
      printf("oh geeze- way to many files in this directory\n");
      exit(-1);
     }
    }
    cp+=dp->d_reclen;
   }
  }
  free(buf);

 close(fd);

 qsort(selector->text_lines, len-1, sizeof(char)*MEDBUF, qsort_cmp);

 selector->selected_line = -1;
 cp = (char *)selector->name_object->param.dp1;
 *cp = 0;
 MESSAGE_OBJECT(selector->name_object, MSG_START);
 selector->end = len;


 selector->scroll_bar->param.d2 = len - selector->nlines;
 if(selector->scroll_bar->param.d2 < 0)
  selector->scroll_bar->param.d2 = 0;
 for(i=0;i<selector->nlines;i++) {
  if(i<len) { 
   selector->lines[i]->param.d2 = 0;
   selector->lines[i]->param.dp2 = (void *)selector->text_lines[i]; 
  }
  else
   selector->lines[i]->param.d2 = NO_TEXT;
 }
 selector->scroll_bar->param.d1 = 0;
}

Sint32 do_save(struct select_file_t *selector, char *filename) {
 char sorry_text[SMALLBUF];

 if(selector->load_proc(selector, filename) == LOAD_OK_QUIT) {
  selector->blinky->flags |= STOPPED;
  return RET_QUIT;
 } else {
  snprintf(sorry_text, SMALLBUF,"--== Sorry ==--\n*puts down pen* I cannot save this as %s",
    selector->file_type_name);
  alert(gui_screen->w/2, gui_screen->h/2, sorry_text, "OK :C"); 
  return RET_OK;
 }

}

Sint32 do_load(struct select_file_t *selector, char *filename) {
 char sorry_text[SMALLBUF];
 Sint32 fp;

 if((fp = open(filename, O_RDONLY))<0) {
  snprintf(sorry_text, SMALLBUF, "--== Sorry ==--\n"
                                  "*shuffles papers* %s", strerror(errno) );
  alert(gui_screen->w/2, gui_screen->h/2, sorry_text, "OK :.<");
  return RET_OK;
 }
 close(fp); 


 if(selector->load_proc(selector, filename) == LOAD_OK_QUIT) {
  selector->blinky->flags |= STOPPED;
  return RET_QUIT;
 } else 
  return RET_OK;
}

Sint32 ok_cancel(struct object_t *obj, Sint32 data) {
 struct select_file_t *parent;
 parent = obj->param.dp2;
 parent->blinky->flags |= STOPPED;
 SDL_Delay(1);
 return RET_QUIT;
}

Sint32 ok_do(struct object_t *obj, Sint32 data) {
 char name[BIGBUF];
 struct select_file_t *selector;
 selector = (struct select_file_t *)obj->param.dp2;
 if(selector->name[0] == '/') {
  if(selector->type == LOAD)
   return do_load(selector, selector->name);
  else
   return do_save(selector,selector->name);
 }
 snprintf(name, BIGBUF, "%s/%s", selector->path, selector->name);
 if(selector->type == LOAD)
  return do_load(selector, name);
 else
  return do_save(selector,name);
}

struct select_file_t *setup_overlay_window(Sint32 w, Sint32 h, Sint32 type, char *file_type_name, 
 Sint32 (*load_proc)(struct select_file_t *selector, char *filename)) {
 Sint32 i;
 Sint32 nlines;
 struct select_file_t *new;

 new = (struct select_file_t *)malloc(sizeof(struct select_file_t));
 obj_param_t tmp_parm;
 new->grp = new_group( (gui_screen->w/2) - (w/2),
                       (gui_screen->h/2) - (h/2), w+2, h+2, globl_flags, globl_drop_depth);
 nlines = (h - 85) / 9;
 new->nlines = nlines;
 new->lines = (struct object_t **)malloc(sizeof(struct object_t) * nlines);

 simple_window(new->grp, w,h);
 PARM(8,48, w - 16, (nlines*9)+3 ,&globl_fg, &globl_bg, 0, proc_box);
 
 new_obj(new->grp, &tmp_parm);

 PARM(w-21,50, 11, (nlines*9), &globl_move_color, &globl_bg, SHOW_FOCUS|CALL_BUTTON, proc_scroll_bar);
 tmp_parm.d1 = 0;
 tmp_parm.d2 = 0;
 tmp_parm.dp1 = (void *)new;
 tmp_parm.callback = selector_bot;
 new->scroll_bar = new_obj(new->grp, &tmp_parm);


 for(i=0;i<nlines;i++) { 
  PARM(10,50+(i*9), w-32,9, &globl_fg, &globl_bg, 0, text_hilight);
  tmp_parm.d1 = i;
  tmp_parm.dp1 = new;
  new->lines[i] = new_obj(new->grp,&tmp_parm);
 }


 PARM(25, h-30,100,20,&globl_fg, &globl_bg, SHOW_FOCUS|CALL_BUTTON,proc_button_box);
 new->type = type;
 if(type == LOAD) 
  tmp_parm.dp1 = (void *)"Load :)";
 else
  tmp_parm.dp1 = (void *)"Save ;)";
 tmp_parm.dp2 = (void *)new;
 tmp_parm.callback = ok_do;
 tmp_parm.quit_value = TRUE;
 new_obj(new->grp, &tmp_parm);

 PARM(w-125,h-30,100,20,&globl_fg, &globl_bg, SHOW_FOCUS|CALL_BUTTON,proc_button_box);
 tmp_parm.dp1 = (void *)"Cancel :(";
 tmp_parm.dp2 = (void *)new;
 tmp_parm.callback = ok_cancel;
 tmp_parm.quit_value = FALSE;
 new_obj(new->grp, &tmp_parm);



 new->name[0] = 0;
 PARM(2, 34, w-4,12, &globl_fg, &globl_bg, MAX_CHARS|SHOW_FOCUS, line_edit);
 tmp_parm.dp1 = (void *)new->name;
 new->name_object = new_obj(new->grp, &tmp_parm);

 PARM((w/2), 25, ((w-32)/8),0, &globl_fg, &globl_bg, MAX_CHARS, proc_ctext);
 tmp_parm.dp1 = (void *)new->path;
 new_obj(new->grp, &tmp_parm);

 for(i=0;;i++) {
  new->file_type_name[i] = file_type_name[i];
  if(new->file_type_name[i] == 0) break;
 }
 new->load_proc = load_proc;
 getcwd( new->path, BIGBUF);
 return new;
}

void do_overlay_window(struct select_file_t *selector) {
 void *old_tick;
 gui_timer_t *blinky;
 old_tick = (void *)globl_tick;
 globl_tick = null_tick;
 read_dir(selector);
 blinky = add_timer(selector->name_object, 12, MSG_TICK, 0, selector->grp, ACTIVE_ONLY_WITH_PARENT);
 selector->blinky = blinky;
 group_loop(selector->grp);
 del_timer(blinky);
 globl_tick = old_tick;
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwMACgkQMNO4A6bnBrMqagCfSa2x3eUPdvhlZHIkw0xRMCWu
cQEAn1MtaTfNiaggGezbuHBLgIZT7Cdx
=5ZYC
-----END PGP SIGNATURE-----
*/
