/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include <pthread.h>
#include <SDL.h>
#include "gui_types.h"
#include "link.h"
#include "drop.h"
#include "gui.h"
#include "draw.h"
#include "std_dialog.h"
#include "pixel.h"
#include "timer.h"

pthread_mutex_t mtx;

 SDL_Surface *gui_screen;

 void (*globl_tick)(void);
 void (*globl_wait_tick)(void);

 Sint32 gui_mouse_x, gui_mouse_y;
 struct object_t *focus_obj;

 Sint32 globl_flags;
 Sint32 lock_update;
 Sint32 globl_dirt, globl_quit_value;
 Sint32 globl_drop_depth;
 Sint32 floating_key;
 color_t globl_fg, globl_bg;
 color_t globl_move_color;
 group_t *current_grp;

 /* XXX this is really ugly, make a better solution */
 char shift_map[19][2] = {
  "1!",
  "2@",
  "3#",
  "4$",
  "5%",
  "6^",
  "7&",
  "8*",
  "9(",
  "0)",
  "-_",
  "=+",
  "[{",
  "]}",
  ";:",
  "'\"",
  ",<",
  ".>",
  "/?"
 };


 group_t *new_group(Sint32 x, Sint32 y, Sint32 w, Sint32 h, Sint32 flags, Sint32 drop_d) {
  group_t *new;
  new = (group_t *)malloc(sizeof(group_t));
  new->pos_x = x;
  new->pos_y = y;
  new->w = w;
  new->h = h;
  new->flags = flags;
  new->drop_d = drop_d;


  new->drop_w = w + (w/4);
  new->drop_h = h + (h/4);
  
  new->ready = 0;
  new->objs = (struct object_t *)NULL;
  new->go = 0;
  return new;
 }

 struct object_t *new_obj(group_t *grp, obj_param_t *param) {
  struct object_t *new;
  new = (struct object_t *)malloc(sizeof(struct object_t));
  new->in_focus = FALSE;
  new->clicked = FALSE;
  memcpy(&new->param, param, sizeof(obj_param_t));
  if((Sint32)grp->objs == (Sint32)NULL) {
   grp->objs = new;
   INIT_LIST_HEAD(&grp->objs->node);
  } else
   list_add(&new->node,&grp->objs->node);
 }


void broadcast_group(group_t *grp, Sint32 msg, Sint32 data) {
 struct object_t *walker; 
 walker = grp->objs;
 globl_dirt = 0;
 for(;;) {
  walker->param.proc(msg, walker, data);
  walker = (struct object_t *)walker->node.next;
  if((void *)walker == (void *)grp->objs) break;
 }
 if(globl_dirt == 1) 
  clipped_update(grp->pos_x, grp->pos_y, grp->w, grp->h);
}

Sint32 wait_on_mouse(void) {
 Sint32 ret;
 ret = MOUSE_MOVE;
 SDL_Event event;
 while(SDL_PollEvent(&event) ) {
  switch(event.type) {
   case SDL_QUIT:
    SDL_Quit();
    exit(-1);
    break;
   case SDL_MOUSEMOTION:
    gui_mouse_x = event.motion.x;
    gui_mouse_y = event.motion.y;
    break;
   case SDL_MOUSEBUTTONUP:
    ret = MOUSE_UP;
    break;
   case SDL_MOUSEBUTTONDOWN:
    ret = MOUSE_DOWN;
    break;
   case SDL_KEYDOWN:
    floating_key = event.key.keysym.sym;
    ret = KEY_DOWN;
   break;
  }
 }
 if((Sint32)globl_wait_tick!=-1)
   globl_wait_tick();
 return ret;
}

 void null_tick(void) {
 }

 void default_tick(void) {
  SDL_Delay(10);
 }

/*                  ______ 
 *               .-"      "-. 
 *              /            \ 
 *  _          |              |          _ 
 * ( \         |,  .-.  .-.  ,|         / )
 *  > "=._     | )(__/  \__)( |     _.=" <
 * (_/"=._"=._ |/     /\     \| _.="_.="\_)
 *        "=._ (_     ^^     _)"_.=" 
 *            "=\__|IIIIII|__/="
 *           _.="| \IIIIII/ |"=._
 * _     _.="_.="\          /"=._"=._     _
 *( \_.="_.="     `--------`     "=._"=._/ )
 * > _.="                            "=._ <
 *(_/                                    \_)
 *              NOW HEAR THIS
 *         This function is hacked 
 *       to bits. Clean before hacking
 */

void group_loop(group_t *grp) {
 SDL_Rect drop_clip;
 unsigned char *save_buf, *pix;
 Sint32 i;
 Sint32 key, modstate;
 Sint32 tmp1, tmp2;
 Sint32 clip_x, clip_y, clip_w, clip_h;
 SDL_Event event;
 struct object_t *walker;
 group_t *old_grp;

 old_grp = current_grp;
 current_grp = grp;

 if((grp->pos_x > gui_screen->w) ||
    (grp->pos_y > gui_screen->h) ) return;

 if(CHECK_FLAG(grp->flags, DROP_SHADOW) == TRUE &&
    CHECK_FLAG(grp->flags, DROP_SHADOW_READY) == FALSE) {
  grp->drop_buffer = new_drop(grp->drop_d);
  grp->flags |= DROP_SHADOW_READY;
 }

 if(CHECK_FLAG(grp->flags, DROP_SHADOW) == TRUE) {
  clip_w = grp->drop_w;
  clip_h = grp->drop_h;
 } else {
  clip_w = grp->w;
  clip_h = grp->h;
 }
 clip_x = grp->pos_x;
 clip_y = grp->pos_y;

 if(grp->pos_x < 0) {
  clip_w += grp->pos_x;
  clip_x = 0;
 } else
  if((clip_x + clip_w) >= gui_screen->w ) 
   clip_w = gui_screen->w - clip_x - 1;
//   clip_w -= (clip_x + clip_w) - gui_screen->w + 1;

 if(grp->pos_y < 0) {
  clip_h += grp->pos_y;
  clip_y = 0;
 } else
  if((clip_y + clip_h) > gui_screen->h ) 
   clip_h -= (clip_y + clip_h) - gui_screen->h;

 save_buf=(unsigned char *)malloc((clip_w*clip_h)*gui_screen->format->BytesPerPixel);
 pix = (unsigned char *)((Uint8 *)gui_screen->pixels +
		  (clip_y * gui_screen->pitch) +
		  (clip_x * gui_screen->format->BytesPerPixel) );

 LOCK;

 /* XXX we crash here sometimes 
  * because the entire clipping area is bigger then the screen :( */


 if((clip_h - clip_y)>gui_screen->h)
  clip_h = gui_screen->h - clip_y;
 for(i=0;i<clip_h;i++) 
  memcpy(
    (Uint8 *)((Uint8 *)save_buf+(i*(clip_w*gui_screen->format->BytesPerPixel))),
    (Uint8 *)((Uint8 *)pix +    (i*gui_screen->pitch)),
				 clip_w*gui_screen->format->BytesPerPixel);
 UNLOCK;

 if(CHECK_FLAG(grp->flags, DROP_SHADOW) == TRUE) {
  drop_clip.w = grp->w;
  drop_clip.h = grp->h; 
   draw_drop(gui_screen, grp->pos_x, grp->pos_y,grp->drop_buffer,grp->drop_w, grp->drop_h,&drop_clip);
  }

 
 if(grp->ready == 0) {
  broadcast_group(grp, MSG_START, NULL);
  grp->ready = 1;
 }

  walker = grp->objs;
   if((Sint32)walker != (Sint32)NULL){
    for(;;) {
     if(gui_mouse_x > (grp->pos_x + walker->param.x)                   &&
        gui_mouse_x < (grp->pos_x + walker->param.x + walker->param.w) &&
        gui_mouse_y > (grp->pos_y + walker->param.y)                   &&
        gui_mouse_y < (grp->pos_y + walker->param.y + walker->param.h)) {
      if(walker->in_focus == FALSE) 
	 walker->in_focus = TRUE;
      } else
      if(walker->in_focus == TRUE) 
       walker->in_focus = FALSE;

     walker = (struct object_t *)walker->node.next;
     if((void *)walker == (void *)grp->objs) break;
    }
   }

 broadcast_group(grp, MSG_DRAW, NULL);

 clipped_update(0,0,0,0);

 broadcast_group(grp, MSG_AFTERDRAW, NULL);


 for(;;) {
  globl_dirt = 0;
  while(SDL_PollEvent(&event) ) {
   switch(event.type) {
    case SDL_QUIT:
     SDL_Quit();
     exit(-1);
    break;
    case SDL_MOUSEMOTION:
     gui_mouse_x = event.motion.x;
     gui_mouse_y = event.motion.y;
     walker = grp->objs;
     if((Sint32)walker != (Sint32)NULL){
      for(;;) {
       if(gui_mouse_x > (grp->pos_x + walker->param.x)                   &&
          gui_mouse_x < (grp->pos_x + walker->param.x + walker->param.w) &&
          gui_mouse_y > (grp->pos_y + walker->param.y)                   &&
	  gui_mouse_y < (grp->pos_y + walker->param.y + walker->param.h)) {
        if(walker->in_focus == FALSE) {
         walker->in_focus = TRUE;
        if(walker->param.proc(MSG_INFOCUS, walker, NULL) == RET_QUIT) 
         goto done1;
        } else
        if(walker->param.proc(MSG_MOUSEMOVE,walker,NULL) == RET_QUIT)
         goto done1;
       } else
       if(walker->in_focus == TRUE) {
          walker->in_focus = FALSE;
        if(walker->param.proc(MSG_OUTFOCUS, walker, NULL) == RET_QUIT)
	   goto done1;
       }

       walker = (struct object_t *)walker->node.next;
       if((void *)walker == (void *)grp->objs) break;
      }
     }

     break;
     case SDL_MOUSEBUTTONDOWN:
      if(event.button.button == SDL_BUTTON_RIGHT) {
       walker = grp->objs;
       for(;;) {
	if(gui_mouse_x > (grp->pos_x + walker->param.x) &&
	   gui_mouse_x < (grp->pos_x + walker->param.x + walker->param.w) &&
	   gui_mouse_y > (grp->pos_y + walker->param.y) &&
	   gui_mouse_y < (grp->pos_y + walker->param.y + walker->param.h)) {
	 if(walker->param.proc(MSG_RIGHT, walker, NULL) == RET_QUIT)
	  goto done1;
	}
	walker = (struct object_t *)walker->node.next;
	if((void *)walker == (void *)grp->objs) break;
       }	
       break;
      }
     case SDL_MOUSEBUTTONUP:
      walker= grp->objs;
      for(;;) {
       if(walker->in_focus == TRUE) {
	if(event.type == SDL_MOUSEBUTTONDOWN) {
	 walker->clicked = TRUE;
	 if(walker->param.proc(MSG_CLICK, walker, NULL) == RET_QUIT) 
	  goto done1;
	} else {
	 if(walker->clicked == TRUE) { 
	  walker->clicked = FALSE;
	  if(walker->param.proc(MSG_PRESS, walker, NULL) == RET_QUIT)
	   goto done1;
	 }
	 if(walker->param.proc(MSG_UNCLICK, walker, NULL) == RET_QUIT)
	  goto done1;
	}
       }
       walker = (struct object_t *)walker->node.next;
       if((void *)walker == (void *)grp->objs) break;
      }
     break;
     case SDL_KEYUP:
     case SDL_KEYDOWN:
      walker = grp->objs;
      for(;;) {
       if(walker->in_focus == TRUE) {
	if(event.type == SDL_KEYUP) {
	 key = event.key.keysym.sym;
	 if( key<SDLK_NUMLOCK || key > SDLK_COMPOSE) {
	  modstate = SDL_GetModState();
	  if( CHECK_FLAG(modstate, KMOD_LSHIFT) == TRUE ||
	      CHECK_FLAG(modstate, KMOD_RSHIFT) == TRUE ||
	      CHECK_FLAG(modstate, KMOD_CAPS) == TRUE) {
	   key = toupper(key);
	   for(i=0;i<19;i++) 
	    if(key == shift_map[i][0]) {
	     key = shift_map[i][1];
	     break;
	    }
	  }
	  if(walker->param.proc(MSG_KEYUP, walker, key) == RET_QUIT)
	   goto done1;
	 }
	} else {
	 key = event.key.keysym.sym;
	 if( key<SDLK_NUMLOCK || key > SDLK_COMPOSE) {
	  modstate = SDL_GetModState();
	  if( CHECK_FLAG(modstate, KMOD_LSHIFT) == TRUE ||
	      CHECK_FLAG(modstate, KMOD_RSHIFT) == TRUE ||
	      CHECK_FLAG(modstate, KMOD_CAPS) == TRUE) {
	   key = toupper(key);
	   for(i=0;i<19;i++) 
	    if(key == shift_map[i][0]) {
	     key = shift_map[i][1];
	     break;
	    }
	  }

	  if(walker->param.proc(MSG_KEYDOWN,walker,key) == RET_QUIT)
	  goto done1;
	 }
	}
       }
       walker = (struct object_t *)walker->node.next;
       if((void *)walker == (void *)grp->objs) break;
      }
     break;
    }
   }
   if(globl_dirt == 1)
    clipped_update(current_grp->pos_x, current_grp->pos_y,
		   current_grp->pos_x+current_grp->w, current_grp->pos_y+current_grp->h);
   if((void *)globl_tick!=-1)
    globl_tick();
   if(grp->go == 0) grp->go++;
  }
 done1:
  LOCK;
  for(i=0;i<clip_h;i++) 
   memcpy( (Uint8 *)((Uint8 *)pix + (i*gui_screen->pitch)),
	   (Uint8 *)((Uint8 *)save_buf+(i*(clip_w*gui_screen->format->BytesPerPixel))),
					       clip_w*gui_screen->format->BytesPerPixel);
  UNLOCK;
  if(globl_quit_value == MOVE_GROUP) 
   clipped_update(walker->param.d1, walker->param.d2, clip_w, clip_h);
  else
   clipped_update(grp->pos_x, grp->pos_y, clip_w, clip_h);

  free(save_buf);


 current_grp = old_grp;
 if(globl_quit_value == MOVE_GROUP)  {
  globl_quit_value = 0;
  group_loop(grp);
 }
}

void destroy_group(group_t *grp) {
 struct object_t *walker;
 struct object_t *prev;

 broadcast_group(grp, MSG_DESTROY, NULL);

 walker = grp->objs;
 for(;;) {
  prev = walker;
  walker = (struct object_t *)walker->node.next;
  free(prev);
  if((void *)walker == (void *)grp->objs) break; 
 }

}

void init_gui(Sint32 x,Sint32 y, Sint32 flags) {
 Sint32 sdl_flags;
 SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO);
 init_timers();
 globl_tick = default_tick;
 globl_wait_tick = -1;
 sdl_flags = SDL_SWSURFACE;
 if(CHECK_FLAG(flags,FULLSCREEN)== TRUE) sdl_flags |= SDL_FULLSCREEN;

 if((gui_screen = SDL_SetVideoMode(x, y, 24, sdl_flags))<=0)
  err(1,"could not get screen :(\n");
 printf("screen is 0x%lx\n", gui_screen);
 SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
 gc = gui_screen;
 lock_update = 0;
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgv4ACgkQMNO4A6bnBrMztACeJgCO7Zd+U3v4dF4stHB7m7Bu
72MAnRX3GOhyEjfprSEoSkFlnch9sWC9
=ejY6
-----END PGP SIGNATURE-----
*/
