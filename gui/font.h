/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#define CENTER_OF_STRING(a)	(((strlen(a)*8)/2))

#define CR_TERMINAL	8

extern unsigned char default_font[];

extern unsigned char *font;

void draw_char(Sint32 x, Sint32 y, char c, Sint32 fg_color, Sint32 bg_color, Sint32 flags);
void draw_text(Sint32 x, Sint32 y, char *in, color_t *fg, color_t *bg, Sint32 hash, Sint32 max);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgv0ACgkQMNO4A6bnBrNVFgCfdKjgqVQD3dlU+Ur4HA5Bkoww
JLUAn2qJNjJcmsfdLkNznIxSqMmIliPg
=jiha
-----END PGP SIGNATURE-----
*/
