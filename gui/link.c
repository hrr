/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "config.h"
#include "link.h"

void __list_add(struct list_head *new,
                struct list_head *prev,
		struct list_head *next) {
 next->prev = new;
 new->next = next;
 new->prev = prev;
 prev->next = new;
}

void list_add(struct list_head *new, struct list_head *head) {
 __list_add(new,head,head->next);
}

void __list_del(struct list_head *prev, struct list_head *next) {
 next->prev = prev;
 prev->next = next;
}

void list_del(struct list_head *entry) {
 __list_del(entry->prev, entry->next);
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwIACgkQMNO4A6bnBrNBrgCfb/VxRKBOQ0rk+kpEeQB/DyuG
puUAnR0nZ5LPlbRs3VzjUAwg4JmoQ1Y7
=mnYY
-----END PGP SIGNATURE-----
*/
