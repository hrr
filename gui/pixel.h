/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#define BIG_PIXEL_DOWN_8		pix[0] = src[3] \
 					pix++

#define BIG_PIXEL_DOWN_16		pix[0] = src[2]; \
	     		           	pix++; \
                        		pix[0] = src[3]; \
					pix++

#define BIG_PIXEL_DOWN_24		pix[0] = src[1]; \
			        	pix++; \
                	        	pix[0] = src[2]; \
                        		pix++; \
                  	        	pix[0] = src[3]; \
					pix++;

#define BIG_PIXEL_DOWN_8_STATIC		pix[0] = src[3] 

#define BIG_PIXEL_DOWN_16_STATIC	pix[0] = src[2]; \
		                	pix++; \
        	                	pix[0] = src[3]; \
					pix--

#define BIG_PIXEL_DOWN_24_STATIC	pix[0] = src[1]; \
			        	pix++; \
                	        	pix[0] = src[2]; \
                        		pix++; \
                        		pix[0] = src[3]; \
                 		       	pix-=2



#define BIG_PIXEL_UP_8			src[3] = (Uint8 *)*pix; \
					pix++

#define BIG_PIXEL_UP_16			src[2] = (Uint8 *)*pix; \
					pix++; \
					src[3] = (Uint8 *)*pix; \
					pix++

#define BIG_PIXEL_UP_24			src[1] = (Uint8 *)*pix;\
					pix++; \
		        		src[2] = (Uint8 *)*pix; \
					pix++; \
					src[3] = (Uint8 *)*pix; \
					pix++

#define BIG_PIXEL_UP_8_STATIC		src[3] = (Uint8 *)*pix

#define BIG_PIXEL_UP_16_STATIC		src[2] = (Uint8 *)*pix; \
					pix++; \
					src[3] = (Uint8 *)*pix; \
					pix--

#define BIG_PIXEL_UP_24_STATIC		src[1] = (Uint8 *)*pix;\
					pix++; \
		        		src[2] = (Uint8 *)*pix; \
					pix++; \
					src[3] = (Uint8 *)*pix; \
					pix-=2



#define LITTLE_PIXEL_DOWN_8		pix[0] = src[0] \
 					pix++

#define LITTLE_PIXEL_DOWN_16		pix[0] = src[0]; \
	                		pix++; \
                        		pix[0] = src[1]; \
					pix++

#define LITTLE_PIXEL_DOWN_24		pix[0] = src[0]; \
		        		pix++; \
                        		pix[0] = src[1]; \
                        		pix++; \
                        		pix[0] = src[2]; \
					pix++;

#define LITTLE_PIXEL_DOWN_8_STATIC	pix[0] = src[0] 

#define LITTLE_PIXEL_DOWN_16_STATIC	pix[0] = src[0]; \
	                		pix++; \
                        		pix[0] = src[1]; \
					pix--

#define LITTLE_PIXEL_DOWN_24_STATIC	pix[0] = src[0]; \
		        		pix++; \
                        		pix[0] = src[1]; \
                        		pix++; \
                        		pix[0] = src[2]; \
                        		pix-=2



#define LITTLE_PIXEL_UP_8		src[0] = (Uint8 *)*pix; \
					pix++

#define LITTLE_PIXEL_UP_16		src[0] = (Uint8 *)*pix; \
					pix++; \
					src[1] = (Uint8 *)*pix; \
					pix++

#define LITTLE_PIXEL_UP_24		src[0] = (Uint8 *)*pix;\
					pix++; \
		        		src[1] = (Uint8 *)*pix; \
					pix++; \
					src[2] = (Uint8 *)*pix; \
					pix++

#define LITTLE_PIXEL_UP_8_STATIC	src[0] = (Uint8 *)*pix

#define LITTLE_PIXEL_UP_16_STATIC	src[0] = (Uint8 *)*pix; \
					pix++; \
					src[1] = (Uint8 *)*pix; \
					pix--

#define LITTLE_PIXEL_UP_24_STATIC	src[0] = (Uint8 *)*pix;\
					pix++; \
		        		src[1] = (Uint8 *)*pix; \
					pix++; \
					src[2] = (Uint8 *)*pix; \
					pix-=2
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwYACgkQMNO4A6bnBrMaagCaA/xhB/fcN792v2LFtxSsnbT4
kIcAn090jS4o5+FHQENK3pt1/1wsGuky
=nipk
-----END PGP SIGNATURE-----
*/
