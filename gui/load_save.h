/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#define BIGBUF		2048
#define MEDBUF		256
#define SMALLBUF 	64

#define LOAD_OK_QUIT	1
#define NOPE_TRY_AGAIN	2

#define LOAD	1
#define SAVE	2
#define CLICKED	1
#define NO_TEXT	2


struct select_file_t {
 group_t *grp;
 Sint32 type;
 Sint32 usr_flags;
 char file_type_name[SMALLBUF]; 
 gui_timer_t *blinky;
 Sint32 (*load_proc)(struct select_file_t *selector, char *filename);
 char path[BIGBUF];
 char name[BIGBUF];
 char return_buf[BIGBUF];
 char text_lines[BIGBUF][MEDBUF];
 struct object_t **lines;
 Sint32 offset;
 Sint32 selected_line, nlines;
 Sint32 end;
 struct object_t *scroll_bar;
 struct object_t *name_object;
};


void do_overlay_window(struct select_file_t *selector);
struct select_file_t *setup_overlay_window(Sint32 w, Sint32 h, Sint32 type, char *file_type_name, 
  Sint32 (*load_proc)(struct select_file_t *selector, char *filename));
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwQACgkQMNO4A6bnBrMt/gCeK+ullRbrSFLfjVa3ZHKaT+Wk
MDUAniuPhay8JWA9ZXslasem5/cVUZII
=fy9l
-----END PGP SIGNATURE-----
*/
