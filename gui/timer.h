/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#define ACTIVE_ONLY_WITH_PARENT	1
#define STOPPED			2

typedef struct {
 struct list_head node;
 Sint32 flags;
 struct object_t *obj;
 group_t *parent_grp;
 Sint32 timer; 
 Sint32 reset;
 Sint32 msg, data;
} gui_timer_t;

extern gui_timer_t *globl_timer;
extern Uint32 timer_callback(Uint32 interval);

void init_timers(void);
gui_timer_t *add_timer(struct object_t *obj, Sint32 reset, Sint32 msg, Sint32 data, group_t *parent,Sint32 flags);

void del_timer(gui_timer_t *in);
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgwoACgkQMNO4A6bnBrOmUgCaAmPZGGvogn8s66Z0JaJnMvzO
QFwAoJpWT2Z6unoBwiOkUL5f+kqB2m+F
=23w+
-----END PGP SIGNATURE-----
*/
