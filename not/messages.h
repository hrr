/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
enum {
 NOT_ANNOY_WINDOW,
 NOT_ALERT_WINDOW,
 NOT_PROGRESS_BAR,
 NOT_PROGRESS_BAR_TICK,
 NOT_MESSAGE_BAR,
 NOT_MEMORY_BAR,
 NOT_CUE,
 NOT_SYNC
};
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgtoACgkQMNO4A6bnBrN1pACeIVnq+3sYZzqt08i5SktCMvXE
jNAAn3Wc8dsgP67rMZpHNswOmKNAduMC
=/mlp
-----END PGP SIGNATURE-----
*/
