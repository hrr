/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/
#include "includes.h"

Sint32 coder_return, player_return, host_return;
Sint32 coder_return_ready=0, player_return_ready=0, host_return_ready=0;

Sint32 coder_block = 0, player_block = 0, host_block = 0;


pthread_t *coder_thread, *player_thread, host_thread;

msgstore_t coder_store, player_store, host_store;


void (*bet_coder)(Sint32 msg, void *data, Sint32 len);
void (*bet_player)(Sint32 msg, void *data, Sint32 len);
void (*bet_host)(Sint32 msg, void *data, Sint32 len);


void to_bet_coder(int sigraised, siginfo_t *info, void *a) {
 if(coder_store.msg == NOT_SYNC) return;
 bet_coder(coder_store.msg, coder_store.data, coder_store.len);
}

void to_bet_host(int sigraised, siginfo_t *info, void *a) {
 if(host_store.msg == NOT_SYNC) return;
 bet_host(host_store.msg, host_store.data, host_store.len);
}

void to_bet_player(int sigraised, siginfo_t *info, void *a) {
 if(player_store.msg == NOT_SYNC) return;
 bet_player(player_store.msg, player_store.data, player_store.len);
}


void send_player(Sint32 msg, void *data, Sint32 len, Sint32 type) {
 if((type == DROP) && player_block == 1) return;
 player_store.msg = msg;
 player_store.data = data;
 player_store.len = len;
 player_return_ready = 0;
 if(player_block == 0) 
  pthread_kill(*player_thread, SIG_PLAYER);
 else {
  while(player_block == 1) SDL_Delay(1);
  pthread_kill(*player_thread, SIG_PLAYER);
 }
// SDL_Delay(1);
}

void send_coder(Sint32 msg, void *data, Sint32 len, Sint32 type) {
 if((type == DROP) && coder_block == 1) return;
 coder_store.msg = msg;
 coder_store.data = data;
 coder_store.len = len;
 coder_return_ready = 0;
 if(coder_block == 0) 
  pthread_kill(*coder_thread, SIG_CODER);
 else {
  while(coder_block == 1) SDL_Delay(1);
  pthread_kill(*coder_thread, SIG_CODER);
 }
// SDL_Delay(1);
}

void send_host(Sint32 msg, void *data, Sint32 len, Sint32 type) {
 if((type == DROP) && host_block == 1) return;
 host_store.msg = msg;
 host_store.data = data;
 host_store.len = len;
 host_return_ready = 0;
 if(host_block == 0) 
  pthread_kill(host_thread, SIG_HOST);
 else {
  while(host_block == 1) SDL_Delay(1);
  pthread_kill(host_thread, SIG_HOST);
 }
// SDL_Delay(1);
}

pthread_t *new_thread(Sint32 (*proc)(void)){
 pthread_attr_t type;
 pthread_t *ret;
 if(pthread_attr_init(&type)!=0){
  printf("pthread_attr_init\n");
  exit(-1);
 }

 ret = (pthread_t *)malloc(sizeof(pthread_t));
 pthread_attr_setdetachstate(&type, PTHREAD_CREATE_JOINABLE);
 if(pthread_create(ret, &type, proc, NULL)!=0) {
  printf("can't create thread\n");
  exit(-1);
 }
 return ret;
}


Sint32 init_not(Sint32 type, void (*proc)(Sint32 msg, void *data, Sint32 len)) {
 struct sigaction action;
 sigemptyset(&action.sa_mask);
 action.sa_flags = SA_SIGINFO;
 switch(type) {
  case HOST:
   bet_host = proc;
   host_thread = pthread_self();
   action.sa_handler = to_bet_host;
   sigaction(SIG_HOST, &action, NULL);
   break;
  case PLAYER:
   bet_player = proc;
   action.sa_handler = to_bet_player;
   sigaction(SIG_PLAYER, &action, NULL);
   break;
  case CODER:
   bet_coder = proc;
   action.sa_handler = to_bet_coder;
   sigaction(SIG_CODER, &action, NULL);
   break;

 }
}
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvAACgkQMNO4A6bnBrNwNACdFrw3b4Gwy0wcKXGCR95PBEkd
tqAAn0ODIAm/UUVA2b3wbWqvJNYAnFL5
=MwNl
-----END PGP SIGNATURE-----
*/
