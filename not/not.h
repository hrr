/*
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

  m m mm mmm .----------.  .---------------------. mmm mm m m
  8 8 88 888 | .--------`  |  .------------------` 888 88 8 8
  8 8 88 888 | ```````|`V```````|   |``||``|`````| 888 88 8 8
  8 8 88 888 `------  | |  [] | |``````||  |  [] | 888 88 8 8
  8 8 88 888 |``````  | |     | ````|````  |     | 888 88 8 8
  ` ` `` ``` ``````````````>  |````````````|   |`` ``` `` ` `
                ==============`            `---`
                                 L A B O R A T O R I E S
  
This file is part of Hacker Radio Rec.
  
Hacker Radio Rec is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License or (at your option) any later version.

Hacker Radio Rec is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

               Copyright (C) 2009, Thea DeSilva
  You can find a copy of GNU General Public License in COPYING
*/

#define SIG_CODER	SIGUSR1
#define SIG_PLAYER	SIGVTALRM
#define SIG_HOST	SIGUSR2

#define HOST	0
#define PLAYER	1
#define CODER	2

#define DROP	1
#define STORE	2


typedef struct {
 Sint32 msg, len;
 void *data;
} msgstore_t;

extern Sint32 coder_block, player_block, host_block;

#define SYNCRET_PLAYER \
 while(player_return_ready!=1);

#define SYNCRET_CODER \
 while(coder_return_ready!=1);

#define SYNCRET_HOST \
 while(host_return_ready!=1);

#define PLAYER_RETURN(X) \
 player_return = (X);\
 player_return_ready = 1;

#define HOST_RETURN(X) \
 host_return = (X);\
 host_return_ready = 1;

#define CODER_RETURN(X) \
 coder_return = (X);\
 coder_return_ready = 1;



extern Sint32 coder_return, player_return, host_return;
extern Sint32 coder_return_ready, player_return_ready, host_return_ready;


#define HOST_BLOCK	host_block=1;
#define HOST_UNBLOCK	host_block=0;

#define LIB_BLOCK	lib_block=1;
#define LIB_UNBLOCK	lib_unblock=0;

void send_player(Sint32 msg, void *data, Sint32 len, Sint32 type);
void send_coder(Sint32 msg, void *data, Sint32 len, Sint32 type);
void send_host(Sint32 msg, void *data, Sint32 len, Sint32 type);

extern pthread_t *coder_thread, *player_thread, host_thread; 

pthread_t *new_thread(Sint32 (*proc)(void));

Sint32 init_not(Sint32 type, void (*proc)(Sint32 msg, void *data, Sint32 len));
/*
  Thank you for your attention
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (OpenBSD)

iEYEARECAAYFAkrsgvEACgkQMNO4A6bnBrMFlgCePaqpKjD/OOcuBpmw9JzPu4CO
fsoAn2TMr9vKKJKUMRoVzk5Pcp44B6r3
=zUJt
-----END PGP SIGNATURE-----
*/
